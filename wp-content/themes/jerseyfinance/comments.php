<?
	$args = array(
		'fields' => array(
			'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) . '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
			'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) . '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
		),
		'title_reply'  => '',
		'class_form'	=> 'gform_wrapper',
		'class_submit' => 'c-button red',
		'logged_in_as' => ''
	); 
?>

<div class="wp-comments">
	<div class="container narrow">
		<div class="c-title u-margin-bottom-30">
			Comments
		</div>

		<? if( is_user_logged_in() ): ?>
			<p>Logged in as <a href="/dashboard/update-my-details"><? echo wp_get_current_user()->first_name; ?></a>. <a href="<? echo wp_logout_url('/login'); ?>">Logout.</a></p>
		<? endif; ?>

		<? 
			if ( have_comments() ) : ?>

				<?
					// Show comment form at top if showing newest comments at the top.
					if ( comments_open() ):
						comment_form( $args );
					endif;
				?>

				<ol class="comment-list">
					<? 
						wp_list_comments(array(
							'reverse_top_level' => true
						)); 
					?>
				</ol>

				<?
					// Show comment navigation
					if ( have_comments() ) :
						the_comments_navigation();
					endif;
				?>

				<?
					// If comments are closed and there are comments, let's leave a little note, shall we?
					if ( ! comments_open() ) : ?>
						<p class="no-comments">
							Comments are now closed.
						</p>
						<?
					endif;
				?>

			<? else:

				comment_form( $args );

			endif; 
		?>
	</div>
</div><!-- #comments -->

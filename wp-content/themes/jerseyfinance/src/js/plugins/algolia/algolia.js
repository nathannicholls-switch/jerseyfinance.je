function pushTermToDataLayer() {
	dataLayer.push({ 'event' : "search", 'searchTerm' : $('.ais-SearchBox-input').val() });
}

$(function() {
	'use strict';

	var searchClient = algoliasearch('81UOZBQNLN', 'aa3fa8f300cb3f45cde3ca4f637151a7');

	var search = instantsearch({
		indexName: 'wp_searchable_posts',
		searchClient: searchClient,
		// Don't show initial search if search box is empty
		searchFunction: function(helper) {
			if (helper.state.query === '') {
				// Add helper text
				document.getElementById('site-search__hits').innerHTML = '<p class="search-message">Enter a search term into the box above to get started.</p>';
				
				// Hide pagination and filter containers as there is nothing to display
				$('#site-search__pagination, .site-search__filters').hide();

				// Don't return any results
				return;
			}
			
			// Show pagination and filter containers
			$('#site-search__pagination, .site-search__filters').show();

			// Return results
			helper.search();
		}
	});

	// Search Box
	search.addWidget(instantsearch.widgets.searchBox({
		container: 		'#site-search__searchbox',
		placeholder:	'Enter Keyword...'
	}));

	// Results
	search.addWidget(instantsearch.widgets.hits({
		container: '#site-search__hits',
		cssClasses: {
			list: 'c-feature-grid c-feature-grid__grid-view',
			item: 'c-feature-grid__item small',
		},
		templates: {
			empty: '<p class="search-message">No results were found with your current filters. Try to remove some filters or change the search query.</p>',
			item: function(item) {
				var image = item.results_image ? item.results_image : '/wp-content/uploads/2019/01/JFL-open-graph.jpg';

				var itemHTML = `<div class="c-feature-grid__item-wrapper" onclick="pushTermToDataLayer()">`;

					itemHTML += `<a href="${item.permalink}" class="c-feature-grid__image" style="background-image: url(${image});"></a>`;				

					itemHTML += `<div class="c-feature-grid__content">`;
						itemHTML += `<a class="h5 c-feature-grid__title u-margin-bottom-5" href="${item.permalink}">`;
							itemHTML += item.post_title;
						itemHTML += `</a>`;
						
						if( item.event_date ) {
							itemHTML += `<span class="c-feature-grid__date"><strong>When:</strong> ${item.event_date}</span>`;
						} else if( item.post_type !== 'all-work' ) {
							itemHTML += `<span class="c-feature-grid__date"><strong>Published:</strong> ${item.post_date_formatted}</span>`;
						}

						itemHTML += `<p class="post_categories">${item.post_type_label}</p>`;
					
					itemHTML += `</div>`;
				itemHTML += `</div>`;

				return itemHTML;
			}
		},
	}));

	// Add Pagination	
	search.addWidget(instantsearch.widgets.pagination({
		container: '#site-search__pagination',
		cssClasses: {
			link: 'c-button red',
		}
	}));

	search.addWidget(instantsearch.widgets.refinementList({
		container: '#site-search__post-types',
		attribute: 'post_type_label',
		// Stop the order from changing when the user selects a category
		transformItems(items) {
			var items = items.sort((a, b) => {
				return a.label.localeCompare(b.label)
			});

			return items;
		},
	}));

	search.start();
});
function overflowAnchors() {
	var wrapperWidth 		= $('.post-header__anchors-wrapper').width();
	var linksWidth 		= $('.post-header__anchors-container').width();

	// If the links don't fit, go through each of them, add up the widths until we reach the container size and then shift the extras into a dropdown
	if( linksWidth > wrapperWidth ) {
		var totalWidth = 0;
		
		// Show expand menu
		$('.post-header__anchors-wrapper').addClass('has-overflow');

		$('.post-header__anchors-container a').each(function() {
			var linkClass	= $(this).attr('class');
			totalWidth = totalWidth + $(this).outerWidth(true); // add link width including margins

			// Add this link to the dropdown if it won't fit and it doesn't already exist in the dropdown
			if( totalWidth > wrapperWidth ) {
				// Show the clone in the dropdown
				$('.post-header__anchors-wrapper .expand .' + linkClass).show();

				// Hide the link in the main nav (can't use display: none as we still need to know its width)
				$(this).css({'opacity':'0','visibility':'hidden'});
			} else {
				// Hide the clone in the dropdown
				$('.post-header__anchors-wrapper .expand .' + linkClass).hide();

				// Hide the link in the main nav (can't use display: none as we still need to know its width)
				$(this).css({'opacity':'1','visibility':'visible'});
			}
		});
	} else {
		// Show all links
		$('.post-header__anchors-container a').css({'opacity':'1','visibility':'visible'});

		// Hide expand menu
		$('.post-header__anchors-wrapper').removeClass('has-overflow');
	}
}

$(window).resize(function() {
	overflowAnchors();
});

$(function() {
	overflowAnchors();
})

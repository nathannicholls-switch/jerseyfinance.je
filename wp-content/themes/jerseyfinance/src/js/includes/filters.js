$(function() {

	// Open/close toggle for filters
		$('.c-filters__toggle').click(function() {
			$(this).toggleClass('open');
			$('.c-filters__filters').slideToggle();
		});

	// Show/hide clear form button

		function checkFormValues( form ) {
			// Hide the clear button
			form.find('.c-filters__bar-clear').hide();

			// Serialise all form fields (pushes their data into an array)
			var formValues = form.serializeArray();

			// Loop through the array and check if any have values, if they do, show the clear button
			$.each( formValues, function( i, field ) {
				if( field.value ) {	
					form.find('.c-filters__bar-clear').show();

					// Stop the loop once we get a value
					return false;
				}
			});
		}

		// On load decide whether or not to show clear button
		$(function() {
			// Check if each form has values
			$('.c-filters').each(function() {
				checkFormValues( $(this) );
			});
		});

		// If any of the form fields have a value/are selected, show the clear filters button
		$('.c-filters input').change(function() {
			checkFormValues( $(this).closest('.c-filters') );
			console.log('change');
		});

	// END

	// Filter clear button

		$('.c-filters__bar-clear').click(function() {
			// Hide the clear button
			$(this).hide();

			// Reset all form values (not checkboxes though as they need to just be unchecked)
			$(':input','.c-filters').not(':checkbox, :button, :submit, :reset, #sort-by').val('');

			// Uncheck checkboxes
			$(':checkbox').prop('checked', false).prop('selected', false);

			// Reload items
			updateWithAjax('.c-filters');
		});

	// Grid view button - toggle grid/list class on the containing div to toggle grid/list styles and re-trigger isotope

		$('.c-filters__modifiers-view .view').click(function() {
			if( $(this).hasClass('grid-view') ) {
				$('.item-container').addClass('c-feature-grid__grid-view');
				$('.item-container').removeClass('c-feature-grid__list-view');


				$('.item-container').css('display', 'flex');
				$('.events-container').hide();
				$('.c-filters__modifiers-sort').show();	
			} else if( $(this).hasClass('list-view') ) {
				$('.item-container').removeClass('c-feature-grid__grid-view');
				$('.item-container').addClass('c-feature-grid__list-view');	

				$('.item-container').css('display', 'block');
				$('.events-container').hide();
				$('.c-filters__modifiers-sort').show();	
			} else if( $(this).hasClass('calendar-view') ) {
				eventsCalendar();							
			}

			$('.c-filters__modifiers-view .view').removeClass('selected');
			$(this).addClass('selected');
		})

});
$(function() {
	var ajaxRunning = false;

	// Pagination click event
		$("#ajax-container").on("click", ".pagination a", function(){
			
			// Only load in new items if ajax isn't currently running
			if( ajaxRunning == false ) {
				// Set to true so that the ajax doesn't run multiple times if clicked twice
				ajaxRunning = true;

				var pagination = $(this).closest('.pagination-container');

				// Grab the URL we need to load over AJAX
				var ajaxUrl		= $(this).attr('href');

				// Add ?ajax=true to the ajax request to stop caching issues
				if( ajaxUrl.indexOf("?") !== -1 ) {
					ajaxUrl = ajaxUrl + '&ajax=true';
				} else {
					ajaxUrl = ajaxUrl + '?ajax=true';		
				}

				// Change button text to loading
				$(this).html('Loading...');

				// console.log(ajaxUrl);

				// Run the ajax request
				ajaxLoading = $.ajax({
					url: ajaxUrl,
					dataType: 'html',
					cache: true
				})
				.done(function (data) {

					// console.log('done');
					test = $(data);

					// Add the grid itmes the ajax item container
					$('#ajax-container .item-container').append( $(data).filter('div').not('.pagination') );

					// Remove old pagination once finished
					if( $(data).filter('.pagination').length ) {
						pagination.html( $(data).filter('.pagination')[0].outerHTML );
						//console.log('New pagination');
					} else {
						pagination.find('.pagination').remove();
						//console.log('Out of posts, remove pagination');
					}

					// Refire countdown timers if they exist
					if( typeof start_countdown_timers == 'function' ) {
						start_countdown_timers();
					}

					// When done, set to false
					ajaxRunning = false;
				});
			
			}

			return false;
		});
});
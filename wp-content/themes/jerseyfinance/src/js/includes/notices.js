$(function() {
	// Loop through each notice on the page and if the user has not previously dismissed them, add a class to show them
	$('.notice').each(function() {
		var cookieName = $(this).data('cookie');

		if( getCookie(cookieName) !== 'closed' ) {
      	$(this).addClass('show');
   	}
	});

	// When a close button is clicked, set a cookie with a specified name and expiry
	$('.notice .close').click(function() {
		var notificationElement = $(this).closest('.notice');
		var cookieName 			= notificationElement.data('cookie');

		// Assumes expiry is set in days
		if( notificationElement.data('expiry') ) {
			// Take current date and add custom expiry
			var cookieExpiry = new Date();
			cookieExpiry.setDate(cookieExpiry.getDate() + notificationElement.data('expiry'));
		} else {
			// Never
			var cookieExpiry = 'Fri, 31 Dec 9999 23:59:59 GMT';
		}

		document.cookie = cookieName + '=closed; expires=' + cookieExpiry + '"; path=/;'

		notificationElement.fadeOut();
	});
});
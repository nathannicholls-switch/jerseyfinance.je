function updateVideoSrc(param, value) {
	$('.c-video iframe').each(function() {
		var iframe 	= $(this);

		// Append the new parameter onto the current iframe src
		var url 		= new URL( iframe.attr('src') );
		url.searchParams.set(param, value);

		// Change iframe src attribute
		iframe.attr('src', url);
	});
}

$('.mute-toggle').click(function() {
	var state = getCookie('muteVideos');

	if( state == 'true' ) {
		document.cookie ='muteVideos=false; expires=Fri, 31 Dec 9999 23:59:59 GMT"; path=/;'

		// Change text on video components to match mute status
		$('.mute-state').html('Yes');
		$('.mute-toggle').html('Mute all videos');

		// Update mute parameters on the embedded videos
		updateVideoSrc('muted', 0);	// Vimeo
		updateVideoSrc('mute', 0);		// Youtube
	} else {
		document.cookie ='muteVideos=true; expires=Fri, 31 Dec 9999 23:59:59 GMT"; path=/;'
		
		// Change text on video components to match mute status
		$('.mute-state').html('No');
		$('.mute-toggle').html('Unmute all videos');

		// Update mute parameters on the embedded videos
		updateVideoSrc('muted', 1); 	// Vimeo
		updateVideoSrc('mute', 1); 	// Youtube
	}
});
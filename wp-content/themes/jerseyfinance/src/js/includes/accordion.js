$(function() {
	$('.c-accordion__title').click(function() {
		$(this).toggleClass('open');
		$(this).next().slideToggle(400);
	});
});
function isInArray(value, array) {
	return array.indexOf(value) > -1;
}

function change_favourite_state( post_id ) {
	// Change data attribute
	$('.add-to-favourites').attr('data-post-id', post_id);

	if( isInArray(post_id, user_favourites) ) {
		$('.add-to-favourites .action__link').html('Remove');
	} else {
		$('.add-to-favourites .action__link').html('Save');
	}
}

$(function() {

	$('.add-to-favourites').click(function() {
		var post_id = $(this).data('post-id');

		var index = user_favourites.indexOf(post_id);

		if (index > -1) {
			user_favourites.splice(index, 1);
		} else {
			user_favourites.push(post_id);
		}

		// The data we want to push to our function
		var data = {
			action: 'add_to_favourites',	// Will run the action `wp_ajax_add_to_favourites`
			post_id: post_id
		};

		// Make an ajax request with our data - will add/remove item from favourites
		jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
			// console.log('Done');
			var label = $('.add-to-favourites .label').html() == 'Save' ? 'Remove' : 'Save' ;
			$('.add-to-favourites .label').html(label);
		});

		return false;
	});

});
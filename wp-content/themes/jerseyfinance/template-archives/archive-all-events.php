<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content">
		<div class="site-wrapper">

			<? get_template_partial( 'includes/archives/filters', array(
				'event_date' 			=> true,
				'related_post_types'	=> array(
					'all-sectors',
					// 'all-markets', // replaced by locations taxonomy
				),
				'taxonomies'	=> array(
					'all-event-locations' 	=> 'term_order',
					'all-authors' 				=> 'term_order',
				),
				'modifiers' => true
			)); ?>

<? endif; ?>

			<? 
				// Includes the container and pagination used for AJAX
				include( locate_template('/includes/archives/ajax-container.php') ); 
			?>

<? if( !request_is_AJAX() ): ?>

			<!-- The element for the calendar. Is initialised once the toggle is clicked -->
			<div class="events-container container" style="display: none; opacity: 0;">
				<div id="events-calendar"></div>
			</div>

			<? 
				// Will search for a page that matches the archive slug and will pull page content from it
				include( locate_template('/includes/archives/archive-page-content.php') ); 
			?>

		</div>
	</div>

<? endif; ?>

<? function fire_full_calendar() { ?>

   <script>

		// Function used to switch to list view on mobile devices
		function checkTheView() {
			if ($(window).width() < 900) {
				$("#events-calendar").fullCalendar("changeView", "listMonth");
			} else {
				$("#events-calendar").fullCalendar("changeView", "month");
			}
		}

		var calendarExecuted = false;

		function eventsCalendar() {
			$('#ajax-container .c-feature-grid').hide();
			$('.events-container').show();
			$('.c-filters__modifiers-sort').hide();

			if (!calendarExecuted) {
				calendarExecuted = true;

				var json_url = '/json/events' + '?' + $('.c-filters').serialize();

				// Fire FullCalendar
				$('#events-calendar').fullCalendar({
					header: {
						left: 'prev, today',
						center: 'title',
						right: 'next'
					},
					eventLimit: 4, // allow "more" link when too many events
					firstDay: 1,
					navLinks: false,
					cache: true,
					events: json_url,
					timezone: "Europe/London",
					timeFormat: "h:mma",
					windowResize: function(view) {
						checkTheView();
					},
					eventAfterAllRender: function(view) {
						// Once all the events have rendedered, show the calendar
						$('.events-container').animate({'opacity':'1'});
					}
				});

				checkTheView();
			}
		}		
   </script>

<? }
add_action('wp_footer', 'fire_full_calendar', 20);
?>
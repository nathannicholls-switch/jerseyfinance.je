<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-bottom-0">
		<div class="site-wrapper">

			<? get_template_partial( 'includes/archives/filters', array(
				'related_post_types'	=> array(
					// 'all-sectors',
					'all-markets',
				),
				'taxonomies' => array(
					'all-business-categories' => 'name'
				)
			)); ?>


			<div class="u-pattern-wrapper fill">
				<div class="container u-position-relative u-padding-top-40 u-padding-bottom-70">
					<div id="ajax-container">
						<div class="c-grid flex ontablet-middle-make-col-6 onmobile-make-col-12 item-container">

	<? endif; ?>

							<? 
								// We push letters into this as we loop through posts, then we output them as anchors at the bottom of the page
								$alphabet_anchors = [];
							?>

							<? // If the request is AJAX make sure no items appear large
								if( have_posts() ):
									while( have_posts() ): the_post();

										include( locate_template( 'includes/archives/business-item.php' ) );
									endwhile;
								else:
									return_no_posts_message();
								endif;
							?>

							<? if( request_is_AJAX() ): ?>
								<? include( locate_template('/includes/archives/pagination.php') ); ?>
							<? endif; ?>

							<div class="alphabet-anchors">
								<div class="container">
									<div class="alphabet-anchors__wrapper">
										<? foreach( $alphabet_anchors as $anchor ): ?>
											<a href="#letter-<? echo $anchor; ?>">
												<? echo $anchor; ?>
											</a>
										<? endforeach; ?>
									</div>
								</div>
							</div>

	<? if( !request_is_AJAX() ): ?>

						</div>

						<div class="pagination-container">
							<? if( !request_is_AJAX() ): ?>
								<? include( locate_template('/includes/archives/pagination.php') ); ?>
							<? endif; ?>
						</div>
					</div>
				</div>
			</div>

			<? 
				// Will search for a page that matches the archive slug and will pull page content from it
				include( locate_template('/includes/archives/archive-page-content.php') ); 
			?>

		</div>
	</div>

<? endif; ?>
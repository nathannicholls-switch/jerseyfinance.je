<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-bottom-0">
		<div class="site-wrapper">

			<? get_template_partial( 'includes/archives/filters', array(
				'publish_date' => true,
				'taxonomies'	=> array(
					'all-work-types' 	=> 'term_order',
					'all-topics'		=> 'name',
				),
				'modifiers' => true
			)); ?>

<? endif; ?>

			<? 
				// Includes the container and pagination used for AJAX
				include( locate_template('/includes/archives/ajax-container.php') ); 
			?>

<? if( !request_is_AJAX() ): ?>

			<? 
				// Will search for a page that matches the archive slug and will pull page content from it
				include( locate_template('/includes/archives/archive-page-content.php') ); 
			?>

		</div>
	</div>

<? endif; ?>
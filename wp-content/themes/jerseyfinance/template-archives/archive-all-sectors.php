<?
	// Will search for a page that matches the archive slug and will pull header content from it
	include( locate_template('/includes/archives/archive-header.php') ); 
?>

<div class="main-content u-padding-bottom-0">
	<div class="site-wrapper">

		<div class="u-pattern-wrapper">

			<div class="container">
				<div class="c-link-grid">
					<div class="c-grid flex ontablet-portrait-make-col-12">

						<? while( have_posts() ): the_post(); ?>
							<? setup_postdata($post); ?>

							<div class="<? echo have_rows('statistics') ? 'c-grid__col-6' : 'c-grid__col-12' ; ?> u-margin-bottom-20">
								<div class="c-sector-item u-box-shadow-light">
									
									<a href="<? the_permalink(); ?>">
										<img class="<? echo have_rows('statistics') ? 'center' : 'left' ; ?>" src="<? echo get_field('header_image')['sizes']['two-column-header-image']; ?>" alt="Jersey Finance" />
									</a>

									<span class="h4 u-margin-bottom-10">
										<a href="<? the_permalink(); ?>">
											<? the_title() ?>&nbsp;›
										</a>
									</span>

									<p class="u-font-size-small">
										<? the_field('summary'); ?>
									</p>

									<? include( locate_template('/includes/singles/all-sectors-stats.php') ); ?>
								
								</div>				
							</div>

						<? endwhile; ?>
						<? wp_reset_postdata(); ?>
					
					</div>
				</div>
			</div>
		
		</div>

		<? 
			// Will search for a page that matches the archive slug and will pull page content from it
			include( locate_template('/includes/archives/archive-page-content.php') ); 
		?>

	</div>
</div>
<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-top-0 u-padding-bottom-0">
		<div class="site-wrapper">

			<? 
				get_template_partial( 'includes/archives/filters', array(
					'taxonomies' => array(
						'all-vacancy-types' 	=> 'name',
						'all-vacancy-terms'	=> 'name',
					)
				));
			?>

<? endif; ?>

			<div class="u-margin-top-30">
				<?
					// Includes the container and pagination used for AJAX (in list view)
					get_template_partial( 'includes/archives/ajax-container', array(
						'grid_state' => 'list'
					));
				?>		
			</div>

<? if( !request_is_AJAX() ): ?>

			<? 
				// Will search for a page that matches the archive slug and will pull page content from it
				include( locate_template('/includes/archives/archive-page-content.php') ); 
			?>

		</div>
	</div>

<? endif; ?>
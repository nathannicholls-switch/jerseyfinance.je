<?	
	// Content security policies etc
	// include('functions/security.php'); 

	// Removes unwanted scripts and styles, and improves ACF admin UI
	include('functions/tidy-wordpress.php'); 

	// Includes our custom scripts and styles
	include('functions/enqueue-files.php'); 

	// Sets up Wordpress (enables titles, menus etc)
	include('functions/setup.php'); 

	// Registers our crop sizes in Wordpress
	include('functions/add-image-sizes.php'); 

	// Shortcodes
	include('functions/shortcodes.php'); 

	// Modify the main queries e.g. posts_per_page, offset, meta_query, tax_query, date_query
	include('functions/modify-pre-get-posts.php'); 
	include('functions/custom/meta-or-title.php');  // must come after `modify-pre-get-posts`

	// Adds descriptions to the main menu (for tooltips)
	include('functions/core/wp-nav-menu-walker.php'); 
	
	// Stop ?paged from redirecting to /page/
	include('functions/core/stop-paged-redirects.php'); 

	// Track post views so that we can display them/sort by views/popularity
	include('functions/core/track-post-views.php'); 

	// Modify plugin behaviours
	include('functions/plugins/acf.php');
	include('functions/plugins/yoast.php');

	// Import functions 
	include('functions/plugins/imports.php');

	// Create JSON feed and enqueue full calendar files
	include('functions/plugins/full-calendar.php');

	// Custom functions
	include('functions/custom/include-breadcrumbs.php'); 
	include('functions/custom/shorten-text.php'); 
	include('functions/custom/add-http.php'); 
	include('functions/custom/ampify-html.php'); 
	include('functions/custom/convert-mime-type.php'); 
	include('functions/custom/get-template-partial.php'); 
	include('functions/custom/request-is-ajax.php'); 
	include('functions/custom/get-terms-by-post-type.php'); 
	include('functions/custom/get-image-id-by-url.php'); 
	include('functions/custom/fetch-latest-date.php');  // sets the "hide" date for events
	include('functions/custom/return-no-posts-message.php'); 


	// Includes functions that change default behaviour and can be used for field conversions, placeholder settings etc.
	include('functions/plugins/gravity-forms.php');

	/* ==== Algolia Searching ==== */
	include('functions/plugins/algolia/add-data-to-search.php');

	// File security 
	include('functions/file-security/file-security.php');

	// Posts that need to be no-index
	include('functions/make-private.php');

	// Functions used in members area/content submission
	include('functions/membership/reset-password-link.php');
	include('functions/membership/can-user-view.php');
	include('functions/membership/working-groups/user-in-working-group.php');
	include('functions/membership/can-user-edit.php');
	include('functions/membership/registration/get-matching-businesses.php');

	// Fetches an array of all users from a business
	include('functions/membership/fetch-all-users.php');

	// Disables dashboard/admin bar for subscribers
	include('functions/membership/disable-subscriber-dashboard.php');

	// Custom Wordpress login/forgotten password
	include('functions/membership/customise-wordpress-login.php');

	/* ==== User and Business Registration ==== */

		// An array containing all GF field ID's and the corresponding ACF field keys
		include('functions/membership/registration/form-fields.php');

		// Fires all the functions that lets GF publish to ACF fields
		// include('functions/membership/registration/business-registration.php');

		include('functions/membership/registration/user-registration.php');

		// Function used by admin-ajax to opt user into an individual business
		include('functions/membership/opt-in.php');

		// Function used by admin-ajax to trash a post from the manage-posts page
		include('functions/membership/trash-user-post.php');

		// Function used by admin-ajax to add a post to a users favourites
		include('functions/membership/favourites/fetch-favourites.php');
		include('functions/membership/favourites/add-to-favourites.php');
	
	/* === END === */

	/* ==== User Update ==== */

		// An array containing all GF field ID's and the corresponding ACF field keys
		include('functions/membership/profile-update/form-fields.php');
		include('functions/membership/profile-update/profile-update.php');
		include('functions/membership/profile-update/confirm-new-email.php');

	/* === END === */

	/* ==== Business Update ==== */

		// An array containing all GF field ID's and the corresponding ACF field keys
		include('functions/membership/business-update/form-fields.php');

		// Functions that modifies the form (sets checkbox values, and changes field types)
		include('functions/membership/business-update/setup.php');

		// Prefill with selected posts values
		include('functions/membership/business-update/prefill-fields.php');

		// Functions that update the post once the form is submitted
		include('functions/membership/business-update/business-update.php');

	/* === END === */

	/* ==== Submit Content ==== */

		// News
		include('functions/membership/submit-news/form-fields.php');
		include('functions/membership/submit-news/submit-news.php');

		// Events
		include('functions/membership/submit-events/form-fields.php');
		include('functions/membership/submit-events/submit-events.php');

		// Vacancies
		include('functions/membership/submit-vacancies/form-fields.php');
		include('functions/membership/submit-vacancies/submit-vacancies.php');

		// Timeline Items
		include('functions/membership/submit-timeline-items/form-fields.php');
		include('functions/membership/submit-timeline-items/submit-timeline-items.php');

	/* === END === */

	// Update dashboard feed
	include('functions/membership/feed-preferences/form-fields.php');
	include('functions/membership/feed-preferences/feed-preferences.php');


	include('functions/membership/salesforce/users-endpoint.php');
	
	// Inlcude script needed for replies
	include('functions/core/setup-comments.php');

	/* ==== Salesforce REST API ==== */
	include('functions/salesforce-api/sf-fetch-user-events.php');

	/* ==== Custom SEO stuff ==== */
	include( 'functions/custom-seo/exclude-from-yoast-sitemap.php' );

	/* ==== Logic that will redireect all users to the homepage if the holding page template is used on the homepage ==== */
	include('functions/holding-page.php');


?>
<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/head.php') ); ?>
<? endif; ?>

<?	
	// Get content from "Page Not Found" page
	$args = array(
		'pagename' => 'page-not-found'
	);

	$page_query = new WP_Query($args);

	if( $page_query->have_posts() ):
		while( $page_query->have_posts() ): $page_query->the_post();
		
			if( locate_template('/template-pages/page-' . $post->post_name . '.php') ):
				include( locate_template('/template-pages/page-' . $post->post_name . '.php') );
			else:
				include( locate_template('/template-pages/page.php') );
			endif;

		endwhile;
	endif;
?>

<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/footer.php') ); ?>
<? endif; ?>
<?
	if( !is_tax() ): 
		
		$archive_page_slug = get_queried_object()->rewrite['slug'];
		
		$args = array(
			'pagename' => $archive_page_slug
		);

		$page_query = new WP_Query($args);

		if( $page_query->have_posts() ):
			while( $page_query->have_posts() ): $page_query->the_post();

				// Include archive header
				include( locate_template('includes/post-headers/one-column-header.php') ); 

			endwhile;
		endif;

		wp_reset_postdata();	
	else: 

		$post = get_queried_object();

		// Include archive header
		include( locate_template('includes/post-headers/one-column-header.php') ); 

		wp_reset_postdata();
	endif;
?>
<div class="c-post-author">
	<? 
		$heading			= get_post_type() == 'all-vacancies' ? 'Employer' : 'Author';
		$business 		= get_field('business');

		if( $business ): 
			$post = $business; 
			setup_postdata( $post );

			if( $post->post_status == 'publish' ):
				
				$logo = get_field('social_logo') ? get_field('social_logo') : get_field('logo'); ?>

				<div class="c-hero-header__contact">
					<div class="c-hero-header__contact-wrapper">
						<?
							$image 			= $logo;
							$name 			= get_the_title();
							$job_title 		= get_field('job_title');
							$email 			= get_field('email_address');
							$direct_line 	= get_field('direct_line');
							$social_media 	= get_sub_field('custom_person')['social_media'];

							get_template_partial( 'includes/partials/key-contact', array(
								'heading'		=> $heading,
								'image' 			=> $image,
								'permalink' 	=> get_the_permalink(),
								'name' 			=> $name,
								'job_title' 	=> $job_title,
								'email' 			=> $email,
								'phone' 			=> $direct_line,
								'crop_image'	=> false // Don't put in circle
							));
						?>
					</div>
				</div>

			<? endif;

			wp_reset_postdata(); ?>
		<? endif; 
	?>
</div>

<div class="c-post-author">

	<?
		$authors = get_field('people');
		
		if( $authors ):
			while( have_rows('people') ): the_row(); ?>

				<div class="c-hero-header__contact">
					<div class="c-hero-header__contact-wrapper">

						<?
							if( get_sub_field('type') == 'jfl' ):

								$post = get_sub_field('person');

								if( $post->post_status == 'publish' ): ?>

									<div class="c-grid__col-4 u-margin-bottom-30" data-aos="fade-up">
								
										<? include( locate_template('includes/partials/key-contact.php') ); ?>
									
									</div>

								<? endif;

								wp_reset_postdata();

							else:

								$image = 			get_sub_field('custom_person')['image'];
								$name = 				get_sub_field('custom_person')['name'];
								$job_title = 		get_sub_field('custom_person')['job_title'];
								$email = 			get_sub_field('custom_person')['email'];
								$direct_line = 	get_sub_field('custom_person')['direct_line'];
								// $social_media = 	get_sub_field('custom_person')['social_media']; ?>

								<div class="c-grid__col-4 u-margin-bottom-30" data-aos="fade-up">

									<?
										get_template_partial( 'includes/partials/key-contact', array(
											'image' 			=> $image,
											'permalink' 	=> false,
											'name' 			=> $name,
											'job_title' 	=> $job_title,
											'email' 			=> $email,
											'phone' 			=> $direct_line,
										));
									?>

								</div>

							<? endif; 
						?>

					</div>
				</div>

			<? endwhile;
		endif;
	?>
</div>
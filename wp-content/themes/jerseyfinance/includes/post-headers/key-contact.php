<div class="c-post-author">
	<? 
		if( !empty($key_contact) && $key_contact->post_status == 'publish' ): 
			$post = $key_contact; 
			setup_postdata( $post ); ?>

			<div class="c-hero-header__contact">
				<div class="c-hero-header__contact-wrapper">
					<?
						get_template_partial( 'includes/partials/key-contact', array(
							'heading'		=> 'Key Contact',
						));
					?>
				</div>
			</div>

			<? wp_reset_postdata(); ?>
		<? endif; 
	?>
</div>
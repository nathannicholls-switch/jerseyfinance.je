<?
	// If events, set date, location and pricing
	if( get_post_type() == 'all-events' ):
		$dates = get_field('dates')[0];

		$start_date = date_create_from_format('d/m/Y', $dates['start_date']); 
		$end_date 	= date_create_from_format('d/m/Y', $dates['end_date']); 
		$date = date_format($start_date, 'l j M Y');
		$date .= $end_date && $dates['start_date'] !== $dates['end_date'] ? ' - ' : '';
		$date .= $dates['start_date'] !== $dates['end_date'] ? date_format($end_date, 'l j M Y') : '';

		$date .= $dates['start_time'] ? ', ' . $dates['start_time'] : '';
		$date .= $dates['end_date'] && $dates['start_time'] !== $dates['end_time'] ? ' - ' : '';
		$date .= $dates['start_time'] !== $dates['end_time'] ? $dates['end_time'] : '';

		$text .= $text ? '<br><br>' : '';
		$text .= $date . '<br><br>';

		$text .= get_field('location') ? '<strong>Location: </strong>' . get_field('location') . '<br>' : '';
		$text .= get_field('pricing') ? '<strong>Pricing: </strong>' . get_field('pricing') : '';
	endif;

	if( get_post_type() == 'all-team' ):
		$heading = get_field('job_title');

		$text .= $text ? '<br><br>' : '';
		$text .= '<p class="u-font-size-small">';
			$text .= get_field('email') ? '<a href="mailto:' . get_field('email') . '">&gt; Email ' . get_the_title() . '</a><br>' : '' ;
			$text .= get_field('telephone') ? '<a href="tel:' . get_field('telephone') . '">&gt; T ' . get_field('telephone') . '</a>' : '' ; 
		$text .= '</p>';
	endif;

	// If title is not set, use term name or page/post title
	if( !$title ):
		$title = is_tax() ? get_queried_object()->name : get_the_title();
	endif;

	// Work with countdowns
	$countdown_date = get_field('countdown_date', false, false);
	if( $countdown_date ):
		$countdown_datetime = new DateTime(get_field('countdown_date', false, false));
	endif;

	if( $countdown_date && $countdown_datetime->format('U') > date('U') ):
		$title = 'Live: ' . $title;
	endif;

	// If heading is not set, use post type name (except for pages and terms)
	if( !$heading ):
		if( get_post_type() == 'all-markets' || get_post_type() == 'all-sectors' ):
			$heading = get_post_type_object( get_post_type() )->label;
		endif;
	endif;

	// Social
	$linkedin 	= get_field('social_media')['linkedin'];
	$facebook 	= get_field('social_media')['facebook'];
	$twitter 	= get_field('social_media')['twitter'];

	if( get_post_type() == 'all-news' ):

		$date_time = get_the_date('j F Y');

		if( get_field('est_read_time') ):
			$date_time .= ' | ';
			$date_time .= '<svg class="duration"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#duration"></use></svg>';			
			$date_time .= 'Est time to read: <strong>' . get_field('est_read_time') . '></strong>';
		endif;

	endif; 

	if( get_post_type() == 'all-work' ):
		$category = get_the_terms(get_the_ID(), 'all-work-types')[0];

		// Output categories and any associated icons
		if( isset($category) ):
			// Check if we want to hide the date for items tagged with this category
			$hide_archive_date = get_field('hide_dates_on_archives', $category);

			if( !$hide_archive_date ):
				$date_time = get_the_date('j F Y');
			endif;
		else:
			$date_time = get_the_date('j F Y');
		endif;
	endif;
?>

<div data-aos="zoom-in-down">

	<? 
		// Output the date at the top if it's a full width background image or video
		if( !empty($date_time) && (get_field('header_style') == 'one-column' && get_field('header_image')) || get_field('header_style') == 'video-background' ): ?>
			<h6 class="<? echo $text_colour; ?> u-margin-bottom-10">
				<? echo $date_time; ?>
			</h6>
		<? endif; 
	?>

	<? if( !empty($heading) ): ?>
		<h6 class="<? echo $text_colour; ?> u-margin-bottom-10">
			<? echo $heading ?>
		</h6>
	<? endif; ?>

	<? if( !empty($title) && !get_field('move_header_content') ): ?>

		<? if( get_post_type() == 'all-businesses' && (!get_field('social_logo') && get_field('logo')) ):
			// Don't output a title (assume their logo is the title)
		else: ?>
			<h1 class="<? echo $text_colour; ?>">
				<? echo $title; ?>
			</h1>
		<? endif; ?>
	<? endif; ?>

</div>

<div class="u-display-inline-block u-width-100" data-aos="zoom-in-up" data-aos-delay="300">

	<? if( !empty($text) ): ?>
		<p class="<? echo $text_colour; ?>">
			<? echo $text; ?>
		</p>
	<? endif; ?>

	<? 
		// Output the date just below the text if it's a one column header with no image, or if its a two column header
		if( !empty($date_time) && (get_field('header_style') == 'one-column' && !get_field('header_image')) || get_field('header_style') == 'two-column' ): ?>
			<span class="u-post-date">
				<? echo $date_time; ?>
			</span>
		<? endif; 
	?>

</div>

<?
	if( $countdown_date ):
		// Include JS countdown timer if field exists and date is in future
		if( $countdown_datetime->format('U') > date('U') ): ?>

			<div class="u-margin-top-20" data-aos="fade-in" data-aos-delay="800">
				<? include( locate_template('includes/partials/work/countdown.php') ); ?>
			</div>
			
		<? endif;
	endif;
?>


<? if( $linkedin || $twitter || $facebook ): ?>

	<div class="social">
		<? if( $linkedin ): ?>
			<a href="<? echo $linkedin; ?>" target="_blank">
				<svg class="linkedin">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use>
				</svg>
			</a>
		<? endif; ?>

		<? if( $twitter ): ?>
			<a href="<? echo $twitter; ?>" target="_blank">
				<svg class="twitter">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use>
				</svg>
			</a>
		<? endif; ?>

		<? if( $facebook ): ?>
			<a href="<? echo $facebook; ?>" target="_blank">
				<svg class="facebook">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
				</svg>
			</a>						
		<? endif; ?>
	</div>

<? endif; ?>

<? // If array is not empty
	if( user_can_view( get_field('members_only') ) ):
		if( $button && array_filter($button) ): ?>
			<a 
				href="<? echo $button['url']; ?>" 
				class="c-button <? echo !empty($image) || !empty($video) ? 'white' : 'red' ; ?> u-margin-top-30 u-margin-bottom-0"
				data-aos="zoom-in" data-aos-delay="300"
				<? echo $button['target'] ? 'target="_blank"' : '' ; ?>>
					<? echo $button['title']; ?>
			</a>
		<? endif; 
	endif; 
?>

<? 
	// Include key contact
	include( locate_template('includes/post-headers/key-contact.php') ); 

	// Include author business
	include( locate_template('includes/post-headers/post-author.php') ); 

	// Inlclude share buttons
	include( locate_template('includes/post-headers/share.php') );
?>
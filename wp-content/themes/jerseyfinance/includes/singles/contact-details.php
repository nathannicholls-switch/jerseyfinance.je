<?
	$name 		= get_field('contact_name');
	$email 		= get_field('contact_email');
	$telephone 	= get_field('contact_telephone');

	if( $name || $email || $telephone ): ?>

		<div class="container u-margin-bottom-40">

			<div class="c-title u-margin-bottom-20">
				Contact Details
			</div>

			<? echo $name ? '<h5>' . $name . '</h5>' : ''; ?>

			<p>
				<? echo $email ? '<strong>Email:</strong>  <a href="tel:' . $email . '">' . $email . '</a><br>' : ''; ?>
				<? echo $telephone ? '<strong>Telephone:</strong> <a href="tel:' . $telephone . '">' . $telephone . '</a>' : ''; ?>
			</p>

		</div>

	<? endif; 
?>
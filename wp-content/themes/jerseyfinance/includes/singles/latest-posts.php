
<?
	// $latest_query is set in the main single template as it's needed for other areas of the template.
	if( $latest_query ):
		if( !request_is_AJAX() ): ?>

			<div id="latest" class="c-tabs__content">
				<div class="main-content">

<? endif; ?>

					<? 
						// Override $wp_query
						$temp_query = $wp_query;
						$wp_query   = NULL; 
						$wp_query = $latest_query;
						
						// Includes the container and pagination used for AJAX
						include( locate_template('/includes/archives/ajax-container.php') ); 

						// Reset $wp_query
						$wp_query = NULL;
						$wp_query = $temp_query;

						wp_reset_postdata();
					?>
					
<? if( !request_is_AJAX() ): ?>

				</div>
			</div>
		
		<? endif; 
	endif; 
?>
<div class="container">
	<div class="c-single-introduction <? echo get_post_type(); ?>">
		
		<div class="c-single-introduction__wrapper">

			<? if( have_rows('statistics') ): ?>

				<div class="c-single-introduction__feature">
					<div class="c-single-introduction__feature-wrapper">
						<div class="c-single-introduction__feature-box u-box-shadow-light">

							<? include( locate_template('includes/singles/all-sectors-stats.php') ); ?>

						</div>				
					</div>
				</div>
			
			<? endif; ?>
			
			<? echo have_rows('statistics') ? '<div class="c-single-introduction__text">' : ''; ?>

				<span class="h6">In brief</span>

				<?
					$post_introduction = get_field('post_introduction');

					$heading 		= $post_introduction['heading'];
					$introduction 	= $post_introduction['introduction'];

					echo $heading ? '<span class="h3">' . $heading . '</span>' : '';

					echo $introduction ? $introduction : '';
				?>

			<? echo have_rows('statistics') ? '</div>' : ''; ?>
		</div>

	</div>
</div>
<?
	// Pagination for singles (markets & user dashboard), takes the current page url and increments the paged parameter on the pagination

	$archive_query = isset($template_args['archive_query']) ? $template_args['archive_query'] : false;
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
?>

<? if( $paged < $archive_query->max_num_pages ): ?>

	<div class="pagination">
		<a class="c-button load-href" href="<? echo get_the_permalink(); ?>?paged=<? echo $paged + 1; ?>">
			Load More
		</a>
	</div>

<? endif; ?>
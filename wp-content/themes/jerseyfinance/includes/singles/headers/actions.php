<div class="post-header__actions">

	<? if( is_user_logged_in() ): 
		// Fetch array of favourites
		$favourites = fetch_favourites(); ?>

		<span class="action post-header__save-post add-to-favourites" data-post-id="<? echo get_the_ID(); ?>">
			<span class="action__link label">
				<? if( in_array(get_the_ID(), $favourites) ): ?>
					Remove
				<? else: ?>
					Save
				<? endif; ?>
			</span>
			<span class="save-dot"></span>
		</span>

	<? endif; ?>

	<div class="action post-header__sharing">
		<span class="action__link">
			Share
		</span>

		<svg class="arrow">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow"></use>
		</svg>

		<ul>
			<li>
				<a href="https://twitter.com/home?status=<? the_permalink(); ?>" target="_blank">
					<svg class="twitter">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use>
					</svg>
				</a>
			</li>
			<li>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<? the_permalink(); ?>" target="_blank">
					<svg class="facebook">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
					</svg>
				</a>
			</li>
			<li>
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<? the_permalink(); ?>&title=<? the_title(); ?>&summary=&source=" target="_blank">
					<svg class="linkedin">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use>
					</svg>
				</a>
			</li>
		</ul>
	</div>
</div>
<!-- Article headers with progress meter -->

<header class="post-header"> 
	<div class="read-progress">
		<div class="progress"></div>
	</div>

	<div class="post-header__wrapper">
		<div class="post-header__title">
			<? the_title(); ?>
		</div>

		<? include( locate_template('/includes/singles/headers/actions.php') ); ?>
	</div>
</header>
<? if( get_field('show_footer_contact_details') ): ?>

	<div class="container">
		<div class="c-text c-text__single" data-aos="fade-up">
			<hr>

			<p class="u-font-size-large">To apply, please submit your CV and a short covering letter supporting your application to:</p>

			<p>Allannah Camsell, Office Manager, at Jersey Finance, 4th floor, Sir Walter Raleigh House, 48-50 Esplanade, St Helier JE2 3QB or email <a href="allannah.camsell@jerseyfinance.je">allannah.camsell@jerseyfinance.je</a> for more information.</p>
		</div>
	</div>

<? endif; ?>
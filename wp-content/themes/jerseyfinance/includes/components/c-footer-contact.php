
<? 
wp_reset_postdata();

	if( !is_archive() ):
		$key_contact = get_field('key_contact'); ?>

		<div class="c-footer-contact <? echo get_post_type() == 'all-events' ? 'u-margin-bottom-50' : '' ; ?>">
			<div class="u-pattern-wrapper">
				<div class="container wide">

					<div class="c-grid ontablet-middle-make-col-12">

						<?
							if( $key_contact ): 
								foreach( $key_contact as $post):
									setup_postdata( $post );
									
									if( $post->post_status == 'publish' ): ?>

										<div class="c-grid__col-4">

											<? include( locate_template('includes/partials/key-contact.php') ); ?>

										</div>
						
									<? endif;
								
								endforeach;
								wp_reset_postdata();
							endif; 
						?>
					
					</div>

				</div>
			</div>
		</div>

	<? endif; 
?>
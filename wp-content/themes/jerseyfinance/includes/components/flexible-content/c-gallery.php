<?
	$images 	= get_sub_field('images');

	if( $images ): ?>
	
		<? 
			// generate a unique ID for this component - used to trigger the AOS animation
			$element_id = 'component-' . md5(uniqid(rand(), true));

			// A delay for AOS that we will increment
			$delay = 0; 
		?>

		<div class="container">
			<div id="<? echo $element_id; ?>" class="c-gallery">
				<div class="c-grid ontablet-middle-make-col-4 onmobile-make-col-6">

					<? foreach( $images as $image ): ?>
						
						<div class="c-grid__col-3" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
							
							<? if( get_query_var( 'amp' ) ): ?>
								<amp-img 
									class="u-margin-bottom-20"
									alt="<? echo $image['alt']; ?>" 
									src="<? echo $image['sizes']['archive-grid-large']; ?>"  
									layout="responsive"  
									width="<? echo $image['sizes']['archive-grid-large-width']; ?>" 
									height="<? echo $image['sizes']['archive-grid-large-height']; ?>"
									lightbox>
								</amp-img>
							<? else: ?>
								<a href="<? echo $image['sizes']['full-width']; ?>">
									<img class="u-box-shadow-light" src="<? echo $image['sizes']['gallery-thumbnails']; ?>" alt="<? echo $image['alt']; ?>" title="<? echo $image['caption']; ?>" />
								</a>
							<? endif; ?>
						
						</div>

						<?
							// increase delay 
							$delay = $delay + 200;
						?>

					<? endforeach; ?>
				
				</div>
			</div>
		</div>

	<? endif;
?>
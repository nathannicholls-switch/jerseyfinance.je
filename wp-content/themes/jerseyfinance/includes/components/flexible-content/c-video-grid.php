<? if( have_rows('videos') ): ?>

	<? 
		// generate a unique ID for this component - used to trigger the AOS animation
		$element_id = 'component-' . md5(uniqid(rand(), true));

		// A delay for AOS that we will increment
		$delay = 0; 
	?>

	<div class="c-video-grid">
		<div class="container">
			<div id="<? echo $element_id; ?>" class="c-video u-margin-bottom-30">

				<div class="c-grid ontablet-middle-make-col-6 onmobile-make-col-12">

					<? while( have_rows('videos') ): the_row(); ?>

						<?
							$iframe = get_sub_field('video');

							if( $iframe ):

								$muted = array_key_exists('muteVideos', $_COOKIE) && $_COOKIE['muteVideos'] == 'true' ? true : false;

								if( $muted ):
									// use preg_match to find iframe src
									preg_match('/src="(.+?)"/', $iframe, $matches);
									$src = $matches[1];

									// Set the video to muted based on user cookie
									$params = array(
										'muted'				=> 1,
										'modestbranding' => 1
									);

									$new_src = add_query_arg($params, $src);

									$iframe = str_replace($src, $new_src, $iframe);  
								endif; ?>

								<div class="c-grid__col-4 u-margin-bottom-30" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
									<div class="c-video__wrapper u-margin-bottom-20">
										<? 
											if( get_query_var( 'amp' ) ):
												$iframe = str_replace('<iframe', '<amp-iframe layout="responsive" sandbox="allow-scripts allow-same-origin"', $iframe);  
												$iframe = str_replace('</iframe', '</amp-iframe', $iframe);  
												echo $iframe;
											else:
												echo $iframe;
											endif; 
										?>
									</div>

									<div class="c-video__details">
										<? if( get_sub_field('title') ): ?>
											<div class="detail <? echo get_sub_field('title') || get_sub_field('duration') ? 'u-margin-bottom-10' : '' ; ?>">
												<strong><? echo get_sub_field('title'); ?></strong>
											</div>
										<? endif; ?>

										<? if( get_sub_field('duration') ): ?>
											<div class="detail">
												<svg class="duration">
													<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#duration"></use>
												</svg>
												Est time to watch: <strong class="u-colour-red"><? echo get_sub_field('duration'); ?></strong>
											</div>
										<? endif; ?>
									</div>

									<? if( !get_query_var( 'amp' ) ): ?>

										<div class="c-video__details">

											<span class="detail">
												Sound: 
												<? echo $muted ? '<strong class="mute-state">No</strong>' : '<strong class="mute-state">Yes</strong>'; ?>
											</span>

											<span class="divider">|</span>

											<span class="detail u-colour-red u-decoration-underline">
												<strong>
													<a href="#" class="mute-toggle" onclick="return false;">
														<? echo $muted ? 'Unmute all videos' : 'Mute all videos'; ?>
													</a>
												</strong>
											</span>
										</div>

									<? endif; ?>
									
								</div>

							<? endif; 
						?>

						<?
							// increase delay 
							$delay = $delay + 200;
						?>

					<? endwhile; ?>

				</div>
			</div>
		</div>
	</div>
<? endif; ?>
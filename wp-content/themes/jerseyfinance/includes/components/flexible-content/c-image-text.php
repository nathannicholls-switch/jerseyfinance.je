<?
	$background_image = get_sub_field('images')['background_image'];
	$image_alignment 	= get_sub_field('align_image');

	// generate a unique ID for this component - used to trigger the AOS animation
	$element_id = 'component-' . md5(uniqid(rand(), true));

	$image_animation 	= $image_alignment == 'left' ? 'fade-right' : 'fade-left';
	$text_animation 	= $image_alignment == 'left' ? 'fade-left' : 'fade-right';
?>

<div class="container">
	<div id="<? echo $element_id; ?>" class="c-image-text <? echo $image_alignment; ?>" <? echo $background_image ? 'style="background-image: url(' . $background_image['sizes']['container-width'] . ')"' : ''; ?>>
		<div class="c-image-text__wrapper">
			<div class="c-image-text__images">
				<?
					// The main two images
					$images = get_sub_field('images')['images'];
				?>

				<? if( isset($images[0]) ): ?>
					<img class="primary-image" src="<? echo $images[0]['sizes']['image-text-primary']; ?>" alt="<? echo $image['alt']; ?>" data-aos-delay="250" data-aos="<? echo $image_animation; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?> />
				<? endif; ?>

				<? if( isset($images[1]) ): ?>
					<div data-aos="<? echo $image_animation; ?>" data-aos-delay="800" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
						<img class="secondary-image u-box-shadow-light" src="<? echo $images[1]['sizes']['image-text-secondary']; ?>" alt="<? echo $image['alt']; ?>" />
					</div>
				<? endif; ?>
			</div>

			<div class="c-image-text__content">
				<div data-aos="<? echo $text_animation; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>

					<?
						// Content group field (contains below fields)
						$content 	= get_sub_field('content');

						// Individual fields
						$heading 	= $content['heading'];
						$title 		= $content['title'];
						$text 		= $content['text'];
						$stats 		= $content['stats'];
						$button 		= $content['button'];
						$quicklinks = $content['quicklinks'];
					?>

					<? if( $heading ): ?>
						<span class="h6">
							<? echo $heading; ?>
						</span>
					<? endif; ?>

					<? if( $title ): ?>
						<span class="h3">
							<? echo $title; ?>
						</span>
					<? endif; ?>

					<? if( $text ): ?>
						<? echo $text; ?>
					<? endif; ?>

					<?	if( $stats ): ?>
						<div class="c-grid onmobile-make-col-12">
						
							<? foreach( $stats as $stat ): ?>

								<div class="c-grid__col-6">

									<?
										// Include individual stat - pass in fields from the repeater inside the group
										get_template_partial('includes/partials/stat', [
											'icon'   	=> $stat['icon'],
											'value'   	=> $stat['value'],
											'text'   	=> $stat['text']
										]); 							
									?>
								
								</div>

							<? endforeach;
							wp_reset_postdata();
							?>
						
						</div>
					<? endif; ?>

					<? if( $button ): ?>
						<a class="c-button red" href="<? echo $button['url']; ?>">
							<? echo $button['title']; ?>
						</a>
					<? endif; ?>

					<? if( $quicklinks ): ?>
						<ul class="c-image-text__quicklinks">
							<? foreach( $quicklinks as $link ): ?>
								<li>
									<a href="<? echo $link['link']['url']; ?>" <? echo $link['link']['target'] ? 'target="_blank"' : '' ; ?>>
										<? echo $link['link']['title']; ?>&nbsp;›
									</a>
								</li>
							<? endforeach; ?>
						</ul>
					<? endif; ?>

				</div>
			</div>

		</div>
	</div>
</div>
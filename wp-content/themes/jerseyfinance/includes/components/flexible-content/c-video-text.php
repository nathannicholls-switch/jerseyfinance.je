<?
	$alignment 	= get_sub_field('align_video');

	// generate a unique ID for this component - used to trigger the AOS animation
	$element_id = 'component-' . md5(uniqid(rand(), true));

	$image_animation 	= $alignment == 'left' ? 'fade-right' : 'fade-left';
	$text_animation 	= $alignment == 'left' ? 'fade-left' : 'fade-right';

	$iframe = get_sub_field('video');

	$muted = array_key_exists('muteVideos', $_COOKIE) && $_COOKIE['muteVideos'] == 'true' ? true : false;

	if( $muted ):
		// use preg_match to find iframe src
		preg_match('/src="(.+?)"/', $iframe, $matches);
		$src = $matches[1];

		// Set the video to muted based on user cookie
		$params = array(
			'muted'			=> 1
		);

		$new_src = add_query_arg($params, $src);

		$iframe = str_replace($src, $new_src, $iframe);  
	endif;
?>

<div class="container">
	<div id="<? echo $element_id; ?>" class="c-image-text <? echo $alignment; ?>">
		<div class="c-image-text__wrapper">
			<div class="c-image-text__images">
				<?
					// The main two images
					$video = get_sub_field('video');
				?>

				<div class="c-video" data-aos="fade-up">
					<div class="c-video__wrapper u-margin-bottom-20">
						<? 
							if( get_query_var( 'amp' ) ):
								$iframe = str_replace('<iframe', '<amp-iframe layout="responsive" sandbox="allow-scripts allow-same-origin"', $iframe);  
								$iframe = str_replace('</iframe', '</amp-iframe', $iframe);  
								echo $iframe;						
							else:
								echo $iframe;
							endif; 
						?>
					</div>		


					<div class="c-video__details">
						<? if( get_sub_field('title') ): ?>
							<div class="detail <? echo get_sub_field('title') || get_sub_field('duration') ? 'u-margin-bottom-10' : '' ; ?>">
								<strong><? echo get_sub_field('title'); ?></strong>
							</div>
						<? endif; ?>

						<? if( get_sub_field('duration') ): ?>
							<div class="detail">
								<svg class="duration">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#duration"></use>
								</svg>
								Est time to watch: <strong class="u-colour-red"><? echo get_sub_field('duration'); ?></strong>
							</div>
						<? endif; ?>
					</div>

					<? if( !get_query_var( 'amp' ) ): ?>

						<div class="c-video__details">

							<span class="detail">
								Sound: 
								<? echo $muted ? '<strong class="mute-state">No</strong>' : '<strong class="mute-state">Yes</strong>'; ?>
							</span>

							<span class="divider">|</span>

							<span class="detail u-colour-red u-decoration-underline">
								<strong>
									<a href="#" class="mute-toggle" onclick="return false;">
										<? echo $muted ? 'Unmute all videos' : 'Mute all videos'; ?>
									</a>
								</strong>
							</span>
						</div>

					<? endif; ?>
				</div>

			</div>

			<div class="c-image-text__content">
				<div data-aos="<? echo $text_animation; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>

					<?
						// Content group field (contains below fields)
						$content 	= get_sub_field('content');

						// Individual fields
						$heading 	= $content['heading'];
						$title 		= $content['title'];
						$text 		= $content['text'];
						$stats 		= $content['stats'];
						$button 		= $content['button'];
						$quicklinks = $content['quicklinks'];
					?>

					<? if( $heading ): ?>
						<span class="h6">
							<? echo $heading; ?>
						</span>
					<? endif; ?>

					<? if( $title ): ?>
						<span class="h3">
							<? echo $title; ?>
						</span>
					<? endif; ?>

					<? if( $text ): ?>
						<? echo $text; ?>
					<? endif; ?>

					<?	if( $stats ): ?>
						<div class="c-grid onmobile-make-col-12">
						
							<? foreach( $stats as $stat ): ?>

								<div class="c-grid__col-6">

									<?
										// Include individual stat - pass in fields from the repeater inside the group
										get_template_partial('includes/partials/stat', [
											'icon'   	=> $stat['icon'],
											'value'   	=> $stat['value'],
											'text'   	=> $stat['text']
										]); 							
									?>
								
								</div>

							<? endforeach;
							wp_reset_postdata();
							?>
						
						</div>
					<? endif; ?>

					<? if( $button ): ?>
						<a class="c-button red" href="<? echo $button['url']; ?>">
							<? echo $button['title']; ?>
						</a>
					<? endif; ?>

					<? if( $quicklinks ): ?>
						<ul class="c-image-text__quicklinks">
							<? foreach( $quicklinks as $link ): ?>
								<li>
									<a href="<? echo $link['link']['url']; ?>" <? echo $link['link']['target'] ? 'target="_blank"' : '' ; ?>>
										<? echo $link['link']['title']; ?>&nbsp;›
									</a>
								</li>
							<? endforeach; ?>
						</ul>
					<? endif; ?>

				</div>
			</div>

		</div>
	</div>
</div>
<?
	if( have_rows('logos') ): ?>

		<? 
			// generate a unique ID for this component - used to trigger the AOS animation
			$element_id = 'component-' . md5(uniqid(rand(), true));

			// A delay for AOS that we will increment
			$delay = 0; 
		?>

		<div class="container">
			<div id="<? echo $element_id; ?>" class="c-logo-grid">
				<div class="c-grid ontablet-middle-make-col-6 onmobile-make-col-12">

					<? while ( have_rows('logos') ) : the_row(); ?>

						<div class="c-grid__col-4 c-logo-grid__item" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>

							<div class="u-table u-full-width">

								<div class="u-table-cell c-logo-grid__logo">
									<? echo get_sub_field('link') ? '<a href="' . get_sub_field('link') . '">' : ''; ?>
										<img class="<? echo !get_sub_field('remove_shadow') ? 'u-box-shadow-light u-padding-10' : ''; ?>" src="<? echo get_sub_field('logo')['sizes']['thumbnail']; ?>" alt="<? the_sub_field('title'); ?>" />
									<? echo get_sub_field('link') ? '</a>' : ''; ?>
								</div>

								<div class="u-table-cell c-logo-grid__details">
								
									<div class="c-logo-grid__title u-font-family-bright u-font-weight-bold u-margin-bottom-15">
										<? 
											echo get_sub_field('link') ? '<a href="' . get_sub_field('link') . '">' : '';
												echo get_sub_field('title') . ' ›'; 
											echo get_sub_field('link') ? '</a>' : '';
										?>
									</div>

									<? echo get_sub_field('text')  ? '<div class="c-logo-grid__text">' . get_sub_field('text') . '</div>' : '' ; ?>
								</div>

							</div>
							
						</div>

						<?
							// increase delay 
							$delay = $delay + 200;
						?>						

					<? endwhile; ?>

				</div>
			</div>
		</div>

	<? endif;
?>
<?
	$is_logged_in = is_user_logged_in();

	// We don't want to have a dropdown on the version inside the slide-out menu
	$menu 	= (isset($template_args['menu']) ? $template_args['menu'] : false); 
?>

<div class="c-account-menu action">
	<? echo !$is_logged_in ? '<a href="/login">' : ''; ?>
		<svg class="login">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#login"></use>
		</svg>

		<? echo $is_logged_in ? '<a href="/dashboard">' : ''; ?>
			<span class="c-account-menu__link-text">
				<? echo !$is_logged_in ? 'login' : 'your account' ; ?>
			</span>
		<? echo $is_logged_in ? '</a>' : ''; ?>
	<? echo !$is_logged_in ? '</a>' : ''; ?>

	<? if( $is_logged_in && $menu ) : ?>
		<div class="c-account-menu__navigation">
			<ul>
				<li><a href="/dashboard/">Dashboard &amp; Feed</a></li>
				<li><a href="/dashboard/update-my-details">Update my Details</a></li>
				<li><a href="/dashboard/change-your-password/">Change your Password</a></li>
				<li><a href="/dashboard/update-feed-preferences">Update your Feed</a></li>
				<li><a href="/dashboard/manage-businesses">Manage Businesses</a></li>
				<li><a href="/dashboard/manage-posts">Manage Posts</a></li>
				<li><a href="/dashboard/saved-items">Saved Items</a></li>
				<li><a href="/dashboard/submit-events/">Submit Events</a></li>
				<li><a href="/dashboard/submit-news/">Submit News</a></li>
				<li><a href="/dashboard/submit-jobs/">Submit Jobs</a></li>
				<li><a href="/dashboard/submit-news-timeline/">Submit to Timeline</a></li>
				<li><a href="/dashboard/working-groups/">Working Groups</a></li>
				<li><a href="<? echo wp_logout_url('/'); ?>">Log Out</a></li>
			</ul>
		</div>
	<? endif; ?>

</div>
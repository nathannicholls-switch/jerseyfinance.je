<?
	$icon 			= (isset($template_args['icon']) ? $template_args['icon'] : get_sub_field('icon')); 
	$value 			= (isset($template_args['value']) ? $template_args['value'] : get_sub_field('value')); 
	$text 			= (isset($template_args['text']) ? $template_args['text'] : get_sub_field('text')); 

	// For Animate on Scroll
	$element_id 	= (isset($template_args['element_id']) ? $template_args['element_id'] : ''); 
	$delay 			= (isset($template_args['delay']) ? $template_args['delay'] : '0'); 
?>

<div class="c-stat">
	<div class="c-stat__wrapper">

		<div class="c-stat__icon">
			<div class="c-stat__icon-wrapper" data-aos="zoom-in" <? echo !empty($delay) ? 'data-aos-delay="' . $delay . '"' : ''; ?>  <? echo !empty($element_id) ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
				<svg class="<? echo $icon; ?> u-fill-red">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<? echo $icon; ?>"></use>
				</svg>
			</div>
		</div>

		<div class="c-stat__content">
			<span class="c-stat__value">
				<? echo $value; ?>
			</span>
			
			<p class="c-stat__text u-font-size-small">
				<? echo $text; ?>
			</p>
		</div>

	</div>
</div>

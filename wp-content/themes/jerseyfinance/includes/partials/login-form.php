
<? 
	if( is_page('permission-denied') ):
		$title = !is_user_logged_in() ? 'Please log in to access this content' : get_field('title');

		get_template_partial( '/includes/post-headers/one-column-header', array(
			'title' 	=> $title
		)); 
	else:

		// Inlcude original header
		$header_style = get_field('header_style') ? get_field('header_style') : 'one-column';
	
		$header_template = locate_template( '/includes/post-headers/' . $header_style . '-header.php' );

		if( $header_template ):
			include( $header_template );
		endif; 

	endif; 
	
	/* ==== Determine where we want to redirect the user once they login ==== */

		$redirect_query_string = filter_input( INPUT_GET, 'redirect', FILTER_SANITIZE_SPECIAL_CHARS ) ? filter_input( INPUT_GET, 'redirect', FILTER_SANITIZE_SPECIAL_CHARS ) : ''; 

		// Setup the redirect once logged in
		if( empty( $redirect_query_string ) ):
			$redirect_url = !is_page('permission-denied') ? get_the_permalink() : '/';
		else:
			$redirect_url = $redirect_query_string;
		endif;

	/* === END === */

	/* ==== Decide which message to output ==== */

		$level 			= get_field('members_only');

		// Change permissions message based on the level of membership required
		if( $level == 'full' ):
			$login_text = 'Sorry, your company must hold Jersey Finance membership and you must have an individual account to be able to access this area. If you have an account, please <a href="/login">login</a>, otherwise please <a href="/membership/join">click here to create one</a>.';
		elseif( $level == 'guest' || !is_user_logged_in() ):
			$login_text = 'Sorry, you must login to the Jersey Finance website to access this area. If you do not have a login, please <a href="/membership/join">create one</a>.';
		elseif( $level == 'ceo' ):
			$login_text = 'Sorry, the business you are registered with must be a member of the Jersey Finance CEO Connect programme to access this area. <a href="/membership/">Find out more.</a>';
		else:
			$login_text = 'Sorry, you do not have permission to view this content.';
		endif;
	
	/* === END === */

	// Grab page content from our permission denied page
	$args = array(
		'pagename' => 'permission-denied'
	);

	$page_query = new WP_Query($args);

	if( $page_query->have_posts() ):
		while( $page_query->have_posts() ): $page_query->the_post(); ?>

			<? 
				// get_template_partial( '/includes/post-headers/one-column-header', array(
				// 	'text' 	=> $login_text
				// )); 
			?>

			<div class="main-content u-padding-top-20">
				<div class="site-wrapper">
					<div class="u-pattern-wrapper">
						<div class="container">
							<div class="custom-login u-box-shadow-light gform_wrapper">
								<div class="custom-login__form">
									<? 	
										// Only output the title on pages that aren't the permission denied template (as we would have double titles)
										if( !is_page('permission-denied') && !is_user_logged_in() ): ?>
											<h4><? echo !empty(get_field('header_content')['title']) ? get_field('header_content')['title'] : get_the_title(); ?></h4>
										<? endif; 
									?>
									
									<p><? echo $login_text; ?></p>

									<? if( !is_user_logged_in() ): ?>
										<?										
											$args = array(
												'redirect' => $redirect_url,
											); 
											
											wp_login_form( $args ); 

										?>

										<? /*
										
										<div class="c-title">Or via LinkedIn</div>
										
										<?
											// Social login plugin
											do_action( 'wordpress_social_login' );
										?>

										*/ ?>
									<? endif; ?>
								</div>

								<div class="custom-login__help">
									<? include( locate_template('/includes/partials/help-text.php') ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<? endwhile; 
	endif; 
?>
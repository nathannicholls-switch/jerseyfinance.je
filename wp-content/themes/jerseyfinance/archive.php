<? if( !request_is_AJAX() ): ?>

	<? include( locate_template('/includes/head.php') ); ?>

<? endif; ?>

	<?
      // Check if there is a custom archive template for this post type, otherwise just default to archive.php
		if( locate_template('/template-archives/archive-' . get_queried_object()->name . '.php') ):
			include( locate_template('/template-archives/archive-' . get_queried_object()->name . '.php') );
		else:
			include( locate_template('/template-archives/archive.php') );
		endif;
	?>

<? if( !request_is_AJAX() ): ?>

	<? include( locate_template('/includes/footer.php') ); ?>

<? endif; ?>
<? include( locate_template('includes/post-headers/member-header.php') ); ?>

<div class="main-content">
	<div class="site-wrapper">

		<? if( get_field('overview') ): ?>

			<div class="container">
				<div class="c-title">
					Overview
				</div>

				<div class="c-text c-text__single">
					<? echo get_field('overview'); ?>
				</div>
				
			</div>
		
		<? endif; ?>

		<?
			if( have_rows('key_contacts') ): ?>

				<div class="container">
					<div class="c-title">
						Key Contacts
					</div>

					<div class="c-grid ontablet-middle-make-col-6 onmobile-make-col-12 flex center u-margin-bottom-20">

						<? while( have_rows('key_contacts') ): the_row(); ?>

							<div class="c-grid__col-4 u-margin-bottom-30">
								<?
									get_template_partial( 'includes/partials/key-contact', array(
										'image' 			=> get_sub_field('image'),
										'permalink' 	=> false,
										'name' 			=> get_sub_field('name'),
										'job_title' 	=> get_sub_field('job_title'),
										'email' 			=> get_sub_field('email'),
										'phone' 			=> get_sub_field('direct_line'),
									));
								?>
							</div>

						<? endwhile; ?>
					
					</div>

				</div>

			<? endif; 
		?>

		<? include( locate_template('includes/singles/businesses/published-content.php') ); ?>

		<? include( locate_template('includes/singles/businesses/related-businesses.php') ); ?>
	
	</div>
</div>
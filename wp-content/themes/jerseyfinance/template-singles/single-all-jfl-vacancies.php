<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<? include( locate_template('/includes/flexible-content.php') ); ?>

		<? include( locate_template('/includes/singles/jfl-vacancies/footer.php') ); ?>
	
	</div>
</div>

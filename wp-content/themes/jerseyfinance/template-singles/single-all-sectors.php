<? if( !request_is_AJAX() ): ?>

	<? 
		// Inlcude post header
		include( locate_template('includes/post-headers/two-column-header.php') ); 
	?>
	
<? endif; ?>

<?
	/* === The query for the "Latest" tab === */
	$meta_query = [];

	// Fetch any posts that have this market tagged under `related markets`
	$meta_query[] = array(
		'key' 		=> 'related_sectors',
		'compare'	=> 'LIKE',
		'value'		=> '"' . get_the_ID() . '"',
	);

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$post_types = filter_input( INPUT_GET, 'post_types', FILTER_SANITIZE_SPECIAL_CHARS, FILTER_REQUIRE_ARRAY ); 

	if( empty( $post_types ) ):
		$post_types = array('all-news','all-events','all-work');
	endif;

	$args = array(
		'post_type' 		=> $post_types,
		'posts_per_page' 	=> 10,
		'meta_query'		=> $meta_query,
		'paged'				=> $paged
	);

	$latest_query = new WP_Query($args);
?>

<? if( !request_is_AJAX() ): ?>

	<?
		// Overview tab
		$tabs['Overview'] = 'overview';

		// Latest tab		
		if( $latest_query->have_posts() ):
			$tabs['Latest'] = 'latest';
		endif;

		// Include content tabs bar using the above tabs
		get_template_partial( 'includes/singles/content-tabs', array(
			'tabs'	=> $tabs
		)); 
	?>

	<div class="site-wrapper">
		<div id="overview" class="main-content c-tabs__content u-padding-top-40">

			<? 
				// Output the introduction + stats/feature
				include( locate_template('/includes/singles/' . get_post_type() . '-introduction.php') ); 
			?>

			<?
				// Output regular post content
				include( locate_template('/includes/flexible-content.php') ); 
			?>

			<? /* ==== The markets that have selected this sector as a related sector ==== */ ?>

				<? 
					// Get all markets that have selected this sector as related
					$markets = get_posts( array(
						'post_type' => 'all-markets',
						'meta_query' => array(
							array(
								'key' => 'sectors', 							// name of custom field
								'value' => '"' . get_the_ID() . '"', 	// matches exactly "123", not just 123. This prevents a match for "1234"
								'compare' => 'LIKE'
							)
						)
					));

					// Output them into the link grid component
					if( $markets ): ?>
				
						<div class="container">
							<div class="c-title">
								Markets served by this sector
							</div>
						</div>

						<div class="container">
							<div class="c-link-grid">
								<div class="c-grid ontablet-portrait-make-col-4 onmobile-make-col-12">

									<? foreach( $markets as $post ): ?>
										<? setup_postdata($post); ?>

										<div class="c-grid__col-3 c-link-grid__item">
									
											<a class="c-link-grid__link u-font-family-bright u-font-weight-bold  u-margin-bottom-10" href="<? the_permalink(); ?>">
												<? the_title() ?>&nbsp;›
											</a>

											<? // echo get_field('summary') ? '<div class="c-link-grid__text u-font-size-small">' . get_field('summary') . '</div>' : '' ; ?>
										
										</div>

									<? endforeach;?>

									<? wp_reset_postdata(); ?>
								
								</div>
							</div>
						</div>
				
					<? endif; 
				?>

			<? /*=== END === */ ?>
		
		</div><!-- Overview -->

		<?
			// Include the grid of related news, events and work
			include( locate_template('/includes/singles/latest-posts.php') );
		?>
	
	</div>

<? endif; ?>

	<?
		// Include the grid of related news, events and work
		include( locate_template('/includes/singles/latest-posts.php') );
	?>

<? if( !request_is_AJAX() ): ?>

</div>



<? endif; ?>
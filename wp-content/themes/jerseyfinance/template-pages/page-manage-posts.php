<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<div class="container">

			<?
				// Businesses that the user can still opt in to
				$current_user_id = get_current_user_id();

				$businesses 	= get_joined_businesses( $current_user_id );

				if( $businesses ):

					$business_ids 	= array_values($businesses);

					// print_r($business_ids);

					$post_types = array(
						'all-news',
						'all-events',
						'all-vacancies'
					);

					// Only fetch posts that were published by a business that the user is part of
						$meta_query = [];

						$meta_query[] = array(
							'key'			=> 'business',
							'value' 		=> $business_ids,
							'compare'	=> 'IN'
						);
					// END

					$has_posts = false;

					foreach( $post_types as $post_type ):

						$args = array(
							'post_type'			=> $post_type,
							'post_status'		=> array('publish', 'pending'),
							'posts_per_page' 	=> -1,
							'meta_query'		=> $meta_query
						);

						$user_posts = new WP_Query( $args );

						if( $user_posts->have_posts() ): 
							$has_posts = true; ?>

							<div class="c-title u-margin-bottom-0">
								<? echo get_post_type_object($post_type)->label; ?>
							</div>
						
							<ul class="button-list u-margin-top-0 c-business-list">

								<? while( $user_posts->have_posts() ): $user_posts->the_post(); ?>

									<? $status = get_post_status( get_the_ID() ); ?>

									<li class="u-table">
										<div class="u-table-cell">
											<<? echo $status == 'publish' ? 'a' : 'span'; ?> class="h5 u-margin-bottom-0" href="<? echo $status == 'publish' ? get_the_permalink() : '#'; ?>">
												<? the_title(); ?> <? echo $status == 'pending' ? '<strong>(pending approval)</strong>' : ''; ?>
											</<? echo $status == 'publish' ? 'a' : 'span'; ?>>
										</div>

										<div class="u-table-cell">
											<a href="#" class="c-button red delete-post" data-post-id="<? echo get_the_ID(); ?>">
												Delete Post
											</a>
										</div>
									</li>

								<? endwhile; ?>
								<? wp_reset_postdata(); ?>

							</ul>

						<? endif;
					endforeach;

					if( !$has_posts ):
						return_no_posts_message();
					endif;

				else:
					return_no_posts_message();
				endif;
			?>

		</div>

		<? include( locate_template('/includes/flexible-content.php') ); ?>

	</div>
</div>


<?
	function delete_user_post() { ?>
		<script>
			$('.delete-post').click(function() {

				// Remove class so that double clicking isn't possible
				$(this).removeClass('delete-post');
				
				var confirmation = confirm("Are you sure you want to delete this post?");

				// Only run function if the user has confirmed the operation
				if( confirmation == true ) {

					var post_id = $(this).data('post-id');

					// The data we want to push to our function
					var data = {
						action: 'trash_user_post',				// Will run the action `wp_ajax_trash_user_post`
						post_id: post_id
					};

					// Make an ajax request with our data - will trash the post
					jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
						// Reload the page once done
						window.location.reload();
					});
				} else {
					// Add the class back on so the click event can work again
					$(this).addClass('delete-post');
					
					// Do nothing
				}

				return false;
			});
	
		</script>
	<? }
	add_action('wp_footer', 'delete_user_post', 20);
?>

<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<div class="container">
			<div class="c-link-grid" data-aos="fade-up">
				<div class="c-grid u-align-centre ontablet-portrait-make-col-4 onmobile-make-col-12">
					<div class="c-grid__col-3 c-link-grid__item">
						<a class="c-link-grid__link u-font-family-bright u-font-weight-bold u-margin-bottom-10" href="/membership/join"> Create your individual account for our website › </a>
					</div>
					<div class="c-grid__col-3 c-link-grid__item">
						<a class="c-link-grid__link u-font-family-bright u-font-weight-bold u-margin-bottom-10" href="/membership/register-your-business/"> Register your business for membership to Jersey Finance › </a>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="c-title">
				Why become a member?
			</div>
		</div>

		<div class="container">
			<div class="c-image-text left">
				<div class="c-image-text__wrapper">
					<div class="c-image-text__images membership-logos">
						<? 
							$args = array(
								'post_type'			=> 'all-businesses',
								'posts_per_page'	=> 9,
								'orderby'        => 'rand',
								'meta_query' => array(
									array(
										'relation' => 'AND',
										array(
											'key' 		=> 'logo',
											'compare' 	=> 'EXISTS',
										),
										array(
											'key' 		=> 'logo',
											'value' 		=> '',
											'compare' 	=> '!=',
										),
										array(
											'key' 		=> 'ceo_connect_status',
											'value'		=> 'yes',
											'compare'	=> '='
										)
									)
								)
							);

							$business_query = new WP_Query( $args );

							if( $business_query->have_posts() ): ?>

								<div class="c-grid">
								
									<? while( $business_query->have_posts() ): $business_query->the_post(); ?>
										<div class="c-grid__col-4 membership-logo">
											<? 
												$image = get_field('social_logo') ? get_field('social_logo') : get_field('logo');
											?>

											<a href="<? the_permalink(); ?>">
												<img class="primary-image" src="<? echo $image['sizes']['logo-full']; ?>" alt="<? the_title(); ?>" data-object-fit="contain">
											</a>
										</div>
									<? endwhile; 
									wp_reset_postdata(); ?>

								</div>

							<? endif; 
						?>
					</div>
					
					<div class="c-image-text__content">
						<span class="h6">Business Benefits</span>
						<span class="h3">
							As the 'voice' of Jersey's international finance centre, Jersey Finance's primary objective is to promote and represent Jersey as jurisdiction of excellence on behalf of it members.
						</span>
						<p>
							We are proud of our relationship with members across all industry sectors, and continue to work closely with key stakeholders including governmental and regulatory bodies, to ensure that Jersey’s current and future interests are kept front of mind and that the value of our industry, both locally and internationally is recognised. This collaborative approach and influential network, along with our extensive programme of events, communications activity and evidence-based research, are key advantages we offer through our membership programme.
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="container u-margin-bottom-60">
			<div class="c-title">
				Member Definition
			</div>
			
			<p>
				To be a Member of Jersey Finance, a Member or a prospective Member must fulfil one of the following conditions:
			</p>
			
			<ul class="checklist">
				<li>
					<p>Is regulated by the Jersey Financial Services Commission and provides financial services in or from Jersey; or</p>
				</li>
				<li>
					<p>Provides financial services in or from Jersey but is not regulated because it can take advantage of an appropriate exemption or is an administrative office that purports to look after one or more families but does not otherwise offer its services to the public; or</p>
				</li>
				<li>
					<p>Offers legal, accounting or other professional services related to financial services.</p>
				</li>
			</ul>
		</div>

		<div id="member-benefits" class="container">
			<div class="c-title">
				Membership Benefits
			</div>

			<div class="c-text">
				<div class="c-text__two-column c-grid onmobile-make-col-12">
					<div class="title c-grid__col-5">
						<div class="c-text__title h3">Compare account types</div>

						<p>If your business qualifies for Jersey Finance membership, you can take advantage of a range of benefits listed here. If you do not meet the membership qualifying criteria, you can still enjoy access to the website as a ‘guest’ and sign up to receive a selection of regular newsletters.</p>


					</div>
					<div class="text c-grid__col-7 u-font-size-small">
						<div class="u-table c-membership-table">
							<div class="u-table-row">
								<div class="u-table-cell">
									
								</div>
								<div class="u-table-cell u-colour-red">
									Member
								</div>
								<div class="u-table-cell">
									Guest
								</div>
							</div>

							<div class="u-table-row">
								<div class="u-table-cell">
									CEO Connect, a  ‘bolt-on’ to membership for CEOs and senior partners (find out more below)
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Attend Jersey Finance events (some may be chargeable)
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Participate in Technical Working Groups and Consultations
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Receive Jersey Finance newsletters providing news, views and updates about our industry
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Access detailed Technical information on website
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Website Access
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Publish certain Press Releases on website
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Opportunities to submit case studies in bespoke Jersey Finance publications and collateral
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Opportunities to publish news on our Community channel
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Participate in Future Connect, a free events programme for those just starting out in the finance industry
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Access discounted and complimentary tickets for certain Third-Party events
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Receive Notice of AGM and associated documents
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Attend and vote at AGM
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Participate in our Life at Finance student work placement programme
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Have a listing in Member Directory
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							<div class="u-table-row">
								<div class="u-table-cell">
									Participate in Community of Interest meetings
								</div>
								<div class="u-table-cell">
									<svg class="tick">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tick"></use>
									</svg>
								</div>
								<div class="u-table-cell">
								</div>
							</div>
							
						</div>

						<a href="/membership/join" class="c-button red u-align-centre u-width-100 u-margin-top-25 u-margin-bottom-20">
							Create your individual account for our website
						</a>
						<br>
						<a href="/membership/register-your-business/" class="c-button red solid u-align-centre u-width-100 ">
							Register your business for membership to Jersey Finance
						</a>
					</div>
				</div>
			</div>
		
		</div>

		<? include( locate_template('/includes/flexible-content.php') ); ?>
	
	</div>
</div>
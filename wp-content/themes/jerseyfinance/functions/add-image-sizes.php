<?
   // Generic Image Sizes
   add_image_size( 'container-width', 1240, 99999, false );				
   add_image_size( 'full-width', 1800, 99999, false );

	// Logos, both thumbnail and full size
	add_image_size( 'logo-square', 450, 450, true );
	add_image_size( 'logo-full', 450, 99999, false );

	// News ticker
	add_image_size( 'logo-small', 72, 99999, false );

   // Full Width Header Background
   add_image_size( 'hero-image', 1800, 940, true );

	// Two Column Header Image
	add_image_size( 'two-column-header-image', 1000, 1000, true ); 	// (2x)
	
	// Gallery Thumbnails
	add_image_size( 'gallery-thumbnails', 255, 160, true );				// (1x)

	// Featured Post Component
	add_image_size( 'featured-post-small', 530, 300, true ); 			// (2x)
	add_image_size( 'featured-post-large', 800, 650, true ); 			// (2x)

	// Image & Text Component
	add_image_size( 'image-text-primary', 920, 960, true ); 				// (2x)
	add_image_size( 'image-text-secondary', 670, 630, true ); 			// (2x)

	// Archive Grid Thumbnails
	add_image_size( 'archive-grid-small', 525, 300, true ); 				// (2x)
	add_image_size( 'archive-grid-medium', 525, 640, true ); 			// (2x)
	add_image_size( 'archive-grid-large', 1110, 640, true ); 			// (2x)
?>
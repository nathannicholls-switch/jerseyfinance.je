<?	
	/*
		This file contains a collection of functions used in the members area of the site
		Some are global settings such as posts_per_page for archives and others are functions for use with Gravity Forms actions

		Summary:
			- a function called to determine whether or not the user is allowed to edit the selected post
			
	*/

	// Change Gravity Forms validation message to something a little more helpful
		function change_message($message, $form){
			return '<div class="validation_failed_message">Validation failed - please fill in all required fields marked with a *</div>';
		}
		add_filter("gform_validation_message", "change_message", 10, 2);

	// Populates a seleceted taxonomy with terms from the entry submission
		function set_post_terms( $entry, $taxonomy, $entry_input_id ) {

			// Save Type against post
			$terms = [];

			foreach( $entry as $key => $value) {
				$type_key = $entry_input_id;

				// Round the key down (as checkboxes return decimals), then check if it matches our entry input id
				if( is_numeric($key) ):
					$key = floor($key);
				endif;
				
				// If the posted key has matches 'input_X_' in it, push the value into the ACF array
				if ( $key == $type_key && $value ) {
					$terms[] = $value;
				}
			}
			wp_set_post_terms( $entry['post_id'], $terms, $taxonomy, false);
		}

	// Fills a dropdown field with businesses the user is attached to
		function populate_business_dropdown( $field, $ceo_connect = false ) {
			$user_id 	= get_current_user_id();
			$choices = [];

			$meta_query = [];

			$meta_query[] = array(
				'relation'	=> 'OR',
				array(
					'key' 		=> 'owner',
					'value'		=> '"' . $user_id . '"',
					'compare'	=> 'LIKE'
				),
				array(
					'key' 		=> 'users',
					'value'		=> '"' . $user_id . '"',
					'compare'	=> 'LIKE'
				),
				array(
					'key' 		=> 'additional_users',
					'value'		=> '"' . $user_id . '"',
					'compare'	=> 'LIKE'
				)
			);
			
			// If we are only fetching CEO businesses, add that to the meta_query
			if( $ceo_connect ):
				$meta_query[] = array(
					'key' 		=> 'ceo_connect_status',
					'value'		=> 'yes',
					'compare'	=> '='
				);
			endif;

			// Get all matching businesses
			$args = array(
				'post_type' 		=> 'all-businesses',
				'posts_per_page'	=> -1,
				'meta_query'		=> $meta_query
			);

			$business_match = new WP_Query($args);

			if( $business_match->have_posts() ):
				while( $business_match->have_posts() ): $business_match->the_post();
					$choices[] = array(
						'text' 	=> get_the_title(),
						'value' 	=> get_the_ID()
					);			
				endwhile;
			endif;

			// Set the field choices
			$field->choices = $choices;

			// if the user has one or less businesses, don't show the field as they have no choice to make.
			// if( count($choices) <= 1 ):
			// 	$field->cssClass = '';
			// endif;
		}

		// If an image is present it will also be used for the two column header image
		function set_header_image( $entry, $header_image_field ) {

			// Set header image
			if( !empty( $entry[$header_image_field['entry_key']] ) ):
				$image_id = attachment_url_to_postid( $entry[$header_image_field['entry_key']] );
				
				// Add image as header image
				update_field( $header_image_field['acf_key'], $image_id, $entry['post_id'] );
				
				// Use two column heading
				update_field( 'field_5bdc2d7964c53', 'two-column', $entry['post_id'] );
			endif;
		}


		function set_post_expiry( $post_id, $expiry ) {
			if( isset($post_id) && isset($expiry) ):

				// Replace any slashes with hyphes so strtotime can handle it
				$expiry = str_replace('/', '-', $expiry);

				// Convert date to Unix timestamp
				$expiry = strtotime( $expiry );

				update_post_meta($post_id, '_expiration-date', $expiry);

				$expiry_options = array(
					'expireType' 	=> 'trash',
					'id'				=> $post_id
				);

				update_post_meta($post_id, '_expiration-date-options', $expiry_options);
				
				update_post_meta($post_id, '_expiration-date-status', 'saved');

			endif;
		}


/* ===== SET VARIABLES FOR USE IN FUNCTIONS BELOW ===== */
	
	$edit_post_id = filter_input( INPUT_GET, 'post-id', FILTER_VALIDATE_INT ); // INT only

/* ===== GRAVITY FORMS - PREFILL FUNCTIONS ===== */

	/*
		FUNCTION: 	prefillGravityFormsList
		PURPOSE: 	This function is used to pre-fill Gravity forms LIST fields when a user is editing a post on the front-end. 
						It grabs the current value from `post_meta` using the field key and formats it as a Gravity Forms friendly array.

		USAGE: 		prefillGravityFormsList( 22, 'dates', array('Start Date', 'End Date') );
	*/
	function prefillGravityFormsList($edit_post_id, $field_key, $columns) { 

		// The currently saved list values in the DB
		$database_values = get_field( $field_key, $edit_post_id );

		if( $database_values ) {

			// Get keys from the columns
			$gf_keys = array_keys( $columns );					// Gravity Forms field names
			$acf_keys = array_keys($database_values[0]); 	// ACF keys from DB

			$list_values = array();      

			$row_count = 0;

         foreach($database_values as $row) {

				$col_count = 0;

				foreach($gf_keys as $column):
					$acf_key = $acf_keys[$col_count]; 	// The ACF key
					$gf_key = $column;						// Gravity Forms key

					// Return different values depending on field type declared in array
					if( !isset($columns[$column]["type"]) ):
						$list_values[$row_count][$gf_key] = $row[$acf_key];
					elseif( $columns[$column]["type"] == 'image' ):
						$list_values[$row_count][$gf_key] = $row[$acf_key]['url'];
					else:
						$list_values[$row_count][$gf_key] = '';
					endif;

					$col_count++;
				endforeach;

				$row_count++;
         }

			// echo '<h5>RESULT</h5>';
			// echo '<pre>';
			// print_r($list_values);
			// echo '</pre>';
			
      } else {
			$list_values = array();   
		}

		return $list_values;
	}

	/*
		FUNCTION: 	prefillBasicGravityFormsField
		PURPOSE: 	This function is used to pre-fill a BASIC Gravity forms field (one that can simply be fetched from the DB and returned e.g. text field) when a user is editing a post on the front-end. 
						It grabs the current value from `post_meta` using the field key and returns it.

		USAGE: 		prefillBasicGravityFormsField( 22, 'name' );
	*/
	function prefillGravityFormsBasicField($edit_post_id, $field_key) {
      if( get_field( $field_key, $edit_post_id ) ):
		   return strip_tags( get_field( $field_key, $edit_post_id ) );
      endif;
	}

	/*
		FUNCTION: 	prefillGravityFormsCheckboxes
		PURPOSE: 	This function is used to pre-fill a CHECKBOX Gravity Forms field when a user is editing a post on the front-end. 
						It grabs the current (ACF) value from `post_meta` using the field key and returns it.

		USAGE: 		prefillGravityFormsCheckboxes( 22, 'sports' );

		IMPORTANT:	This function assumes that the Gravity Forms checkboxes are converted into an ACF object when saved, using the prefillGravityFormsCheckboxes() function. As we are fetching an ACF object and returning it.
	*/
	function prefillGravityFormsCheckboxes($edit_post_id, $field_key) {

		if( get_field( $field_key, $edit_post_id ) ):
			// Get the checkboxes from the post (as an array, not an object)
			$checkbox_values = (array) get_field( $field_key, $edit_post_id );

			// Return only ID
			$checkbox_values = array_column( $checkbox_values, 'ID' );

         return $checkbox_values;
      endif;
	
	}

/* ==== UPDATE FIELD FUNCTIONS ==== */

	function add_flexible_content( $edit_post_id, $text, $video = false, $events = false ) {
		/*
			The ACF function `update_field` doesnt work for repeaters.
			So we add the required stuff to the database via add_post_meta
			This creates a "text" row in the flexible content for the post
		*/

		// Create the array flexible content
		$flex_format = array();

		if( $text ):
			// Events has it's own flexible content so some field keys need changing (sorry for the mess)
			if( !$events ):
				if( $text ):
					$flex_format[] = "text";
				endif;

				if( $video ):
					$flex_format[] = "video";
				endif;

				add_post_meta( $edit_post_id, 'fields', $flex_format, true );

				add_post_meta( $edit_post_id, 'fields_0_two_column', 'no', true );
				add_post_meta( $edit_post_id, '_fields_0_two_column', 'field_5bc6080d175a4', true );
				add_post_meta( $edit_post_id, 'fields_0_text', $text, true );
				add_post_meta( $edit_post_id, '_fields_0_text', 'field_5bc607eb175a2', true );	
				add_post_meta( $edit_post_id, '_fields', 'field_5bc606e81759f', true );
				add_post_meta( $edit_post_id, 'content', '', true );
				add_post_meta( $edit_post_id, '_content', 'field_5bcf4138eb7e9', true );

				if( $video ):
					add_post_meta( $edit_post_id, 'fields_1_reduce_width', '1', true );
					add_post_meta( $edit_post_id, '_fields_1_reduce_width', 'field_5c1bcb133af8d', true );
					add_post_meta( $edit_post_id, 'fields_1_video', $video, true );
					add_post_meta( $edit_post_id, '_fields_1_video', 'field_5bc60fb1f1565', true );
				endif;

				
			else:
				$flex_format[] = "text";

				add_post_meta( $edit_post_id, 'event_fields', $flex_format, true );

				add_post_meta( $edit_post_id, 'event_fields_0_two_column', 'no', true );
				add_post_meta( $edit_post_id, '_event_fields_0_two_column', 'field_5bc6080d175a4', true );
				add_post_meta( $edit_post_id, 'event_fields_0_text', $text, true );
				add_post_meta( $edit_post_id, '_event_fields_0_text', 'field_5bc607eb175a2', true );
				add_post_meta( $edit_post_id, '_event_fields', 'field_5beac8e5e496a', true );

				if( $video ):
					add_post_meta( $edit_post_id, 'event_fields_0_reduce_width', '1', true );
					add_post_meta( $edit_post_id, '_event_fields_0_reduce_width', 'field_5c1bcb133af8d', true );
					add_post_meta( $edit_post_id, 'event_fields_0_video', $video, true );
					add_post_meta( $edit_post_id, '_event_fields_0_video', 'field_5bc60fb1f1565', true );
				endif;
			endif;
		endif;
	}


	function save_event_dates( $edit_post_id, $dates ) {
		global $event_fields;

		// Turn the Gravity Forms data into an actual array
		$dates = maybe_unserialize($dates);

		$acf_dates = array();

		$date_row = array();

		foreach( $dates as $date ):
			foreach( $date as $key => $column):
				$date_row_key = $event_fields['dates']['columns'][$key]['acf_key'];

				if( $key == 'Start Date' || $key == 'End Date' ):
					$column = DateTime::createFromFormat('d/m/Y', $column);
					$column = $column->format('Ymd');
				endif;

				$date_row[$date_row_key] = $column;
			endforeach;

			$acf_dates[] = $date_row;
		endforeach;

		// echo '<pre>';
		// print_r($acf_dates);
		// echo '</pre>';

		update_field( $event_fields['dates']['acf_key'], $acf_dates, $edit_post_id );
	}

	/*
		FUNCTION: 	updateBasicPostContent
		PURPOSE: 	Whenever any form is submitted on the update post page, update the basic post fields (Title and Content).

		USAGE: 		updateBasicPostContent( 22, 'input_1', 'input_6' );
	*/
	function updateBasicPostContent( $edit_post_id, $post_title, $post_content ) {
		global $edit_post_id;
	
		// Update default WP post fields
		$this_post = array(
			'ID'           => $edit_post_id,
			'post_title'   => $_POST[$post_title],
			'post_status'	=> 'pending'
			// can't do post date because that would auto-publish without review
		);

		// Update the post
		wp_update_post( $this_post );
	}

	/*
		FUNCTION: 	updateGravityFormsBasicField
		PURPOSE: 	This function is used to update a BASIC Gravity forms field (one that can simply be replaced in the DB without any additional manipulation).
						It grabs the current value from the posted form and updates the post using `add_post_meta()`

		USAGE: 		updateGravityFormsBasicField( 22, 'input_7', 'first_name' );
	*/
	function updateGravityFormsBasicField( $edit_post_id, $form_field_key, $field_key ) {
		// remove current value
		delete_post_meta( $edit_post_id, $field_key );

		// add new value
		update_field( $field_key, $_POST[$form_field_key], $edit_post_id );
	}

	/*
		FUNCTION: 	updateGravityFormsCheckboxes
		PURPOSE: 	This function is used to update a CHECKBOX Gravity Forms field as an ACF object in the DB. 
						It grabs the current checkbox values from the submitted form and pushes it into an ACF friendly array, then uses `add_post_meta()` to save it to the DB.

		NOTE:			Gravity Forms saves checkbox fields in rows such as `input_12_1`, `input_12_2`, `input_12_3`, where `input_12` is the field and the number afterwards represents a selection.

		USAGE: 		updateGravityFormsCheckboxes( 22, 'input_12', 'sports' );		
	*/
	function updateGravityFormsCheckboxes( $edit_post_id, $form_field_key, $field_name, $acf_key) {
		// remove current value
		delete_post_meta( $edit_post_id, $field_name );
		
		$acf_checkboxes = array();

		// Look for all of the posted values using the forms' field key
		foreach($_POST as $key => $value) {
			
			// If the posted key has matches 'input_X_' in it, push the value into the ACF array
			if (strpos($key, $form_field_key . '_') === 0) {
				array_push($acf_checkboxes, $value);
			}
      }

		// add new value
		//add_post_meta( $edit_post_id, $field_key, $acf_checkboxes );
      update_field( $acf_key, $acf_checkboxes, $edit_post_id  );

	}

	/*
		FUNCTION: 	updateGravityFormsList
		PURPOSE: 	This function is used to update an ACF repeater using values from a Gravity Forms list field (by removing/replacing).
						It grabs the posted form values, formats/groups it and adds using `add_row()`

		USAGE: 		updateGravityFormsList( 22, 'input_12', 'event_dates', 2 );
	*/
	function updateGravityFormsList($edit_post_id, $form_field_key, $field_key, $columns ) {
		global $edit_post_id;

		$column_keys = array_keys($columns);

		// Remove current list items from the DB, so we can replace them (easier than updating with different number of rows etc)
      delete_post_meta($edit_post_id, $field_key);
		
		$submitted_list = $_POST[$form_field_key];

		// echo '<h5>Submitted Data</h5>';
		// echo '<pre>';
		// print_r($submitted_list);
		// echo '</pre>';

      $number_of_columns = count( $column_keys );

		if( $submitted_list ) {

			$value = array();
			$row_count = 0;

			// Group every item in the array and add them into post_meta
			foreach(array_chunk($submitted_list, $number_of_columns) as $list_row) {	
            $col_count = 0;

				// echo '<h5>LIST ROW</h5>';
				// echo '<pre>';
				// print_r($list_row);
				// echo '</pre>';			

				// Check if row has no values, and don't save them
				if( !empty($list_row) && !empty( array_filter($list_row) ) ):

					// Loop through each column and set the value against the key in thr $row array
					foreach( $columns as $column ) {
						// Push values into each of the columns of this row, using the acf key as the array key
						if( !isset($column['type']) ):
					
							// Set field key value in this row to corresponding submitted data
							$value[$row_count][$column['acf_key']] = $list_row[$col_count];

						elseif( $column['type'] == 'image' ):

							// Set field key value in this row to corresponding submitted data image ID
							$image_url = urldecode($list_row[$col_count]);
							$value[$row_count][$column['acf_key']] = attachment_url_to_postid( $image_url );

						else:
							$value[$row_count][$column['acf_key']] = '';
						endif;
						$col_count++;
					}

				endif;

				$row_count++;
			}

			// echo '<h5>ACF Data</h5>';
			// echo '<pre>';
			// print_r($value);
			// echo '</pre>';			
			//die();

			// Push new values into ACF
			update_field( $field_key, $value, $edit_post_id );
		}
	}

?>
<?
	/*
		This function will sort dropdown/checkbox Gravity Forms choices by text value.

		Example usage:

		function pre_event_render( $form ) {

			// Get the form fields
			$fields = $form['fields'];

			// Loop through the form fields
			foreach( $form['fields'] as &$field ) {

				if ( $field->id == '1' ) {
					sort_choices($field);
				}
			}

			return $form;
		}
		add_filter( 'gform_pre_render', 'pre_event_render' );
		add_filter( 'gform_pre_validation', 'pre_event_render' );
		add_filter( 'gform_pre_submission_filter', 'pre_event_render' );
		add_filter( 'gform_admin_pre_render', 'pre_event_render' );
	*/

	function sort_choices( $field ) {
		// Create a new array containing the text values
		$choice_values = array();

		foreach( $field->choices as $key => $row ):
			$choice_values[$key] = $row['text'];
		endforeach;

		// Sort our choices by their text values
		array_multisort($choice_values, SORT_ASC, $field->choices);
	}

?>
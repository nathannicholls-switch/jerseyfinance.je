<?
    /* ==== Force Gravity Forms to init scripts in the footer and ensure that the DOM is loaded before scripts are executed ==== */

      function wrap_gform_cdata_open( $content = '' ) {
         if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
            return $content;
         }
            
         $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
         return $content;
      }
      
      function wrap_gform_cdata_close( $content = '' ) {
         if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
            return $content;
         }

         $content = ' }, false );';
         return $content;  
      }

      add_filter( 'gform_init_scripts_footer', '__return_true' );
      add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open', 1 );
      add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close', 99 );

   /* === END === */

   /* ==== Stop Gravity Forms from scrolling on next/prev ==== */

      function disable_gform_confirmation_anchor($enabled) {
         return false;
      }
      add_filter('gform_confirmation_anchor','disable_gform_confirmation_anchor');

   /* === END === */

	/* ==== Customise Gravity Forms Rich Text Editor ==== */

		function gravity_forms_mce_buttons( $mce_buttons ) {
			$mce_buttons = array( 'bold', 'italic', 'bullist' );
			return $mce_buttons;
		}
		add_filter( 'gform_rich_text_editor_buttons', 'gravity_forms_mce_buttons', 10, 2 );

		function include_dashicons() {
			wp_enqueue_style( 'dashicons' );
		}
		add_filter( 'gform_init_scripts_footer', 'include_dashicons' );

	/* === END === */ 

	/* === Add class to Gravity Forms submit/next/prev buttons */

		function add_solid_button_class( $button, $form ) {
			$dom = new DOMDocument();
			$dom->loadHTML( $button );
			$input = $dom->getElementsByTagName( 'input' )->item(0);
			$classes = $input->getAttribute( 'class' );
			$classes .= " c-button red solid";
			$input->setAttribute( 'class', $classes );
			$new_button = $dom->createElement( 'button' );
			$new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
			$input->removeAttribute( 'value' );
			foreach( $input->attributes as $attribute ) {
				$new_button->setAttribute( $attribute->name, $attribute->value );
			}
			$input->parentNode->replaceChild( $new_button, $input );
		
			return $dom->saveHtml( $new_button );
		}
		add_filter( 'gform_submit_button', 'add_solid_button_class', 10, 2 );

		function add_outline_button_class( $button, $form ) {
			$dom = new DOMDocument();
			$dom->loadHTML( $button );
			$input = $dom->getElementsByTagName( 'input' )->item(0);
			$classes = $input->getAttribute( 'class' );
			$classes .= " c-button red";
			$input->setAttribute( 'class', $classes );
			$new_button = $dom->createElement( 'button' );
			$new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
			$input->removeAttribute( 'value' );
			foreach( $input->attributes as $attribute ) {
				$new_button->setAttribute( $attribute->name, $attribute->value );
			}
			$input->parentNode->replaceChild( $new_button, $input );
		
			return $dom->saveHtml( $new_button );
		}
		add_filter( 'gform_next_button', 'add_outline_button_class', 10, 2 );
		add_filter( 'gform_previous_button', 'add_outline_button_class', 10, 2 );

	/* === END === */


	/* === If a fileupload field is included, add JS for image previewing === */

		function uploadPreview() { ?>
			<script>

				function readURL(input) {
					var previewElement = $(input).siblings('.image-preview');

					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function(e) {
							if( e.target.result ) {
								previewElement.attr('src', e.target.result);
								previewElement.addClass('has-file');
							}
						}

						reader.readAsDataURL(input.files[0]);
					} else {
						previewElement.attr('src', '');
						previewElement.removeClass('has-file');
					}
				}

				$('.gform_wrapper input[type="file"]').change(function() {
					readURL(this);
				});
			</script>
		<? }

		// Adds an empty image tag above all file upload fields
		add_filter( 'gform_field_content', function( $field_content, $field ) {
			if( $field->type == 'fileupload' || $field->inputType == 'fileupload' ) {
				$field_content = str_replace("ginput_container_fileupload'>", "ginput_container_fileupload'><img class='image-preview' />", $field_content);

				add_action( 'wp_footer', 'uploadPreview', 100 );
			}

			return $field_content;
		}, 10, 2 );

	/* === END === */

	/* === Turn list field table elements into divs === */

		add_filter( 'gform_field_content', function( $field_content, $field ) {
			if( !is_admin() ) {

				// If the field type is list (or a custom field, set to list type) convert all table tags to div/class equivalent
				if( $field->type == 'list' || $field->inputType == 'list' ) {
					// Convert table element to div + class
					$field_content = str_replace("<table class='", "<div class='custom_gform_list u-table ", $field_content);
					$field_content = str_replace("</table", "</div", $field_content);

					// Remove thead and tbody elements as they can't be replicated with css
					$field_content = str_replace("<thead>", "", $field_content);
					$field_content = str_replace("</thead>", "", $field_content);
					$field_content = str_replace("<tbody>", "", $field_content);
					$field_content = str_replace("</tbody>", "", $field_content);
					
					// Convert th element to div + class
					$field_content = str_replace("<th", "<div class='u-table-cell'", $field_content);
					$field_content = str_replace("</th", "</div", $field_content);

					// Replace the tr elements with div + class (retains existing classes)
					$field_content = str_replace("<tr class='", "<div class='u-table-row ", $field_content);
					$field_content = str_replace("<tr>", "<div class='u-table-row'>", $field_content);
					$field_content = str_replace("</tr", "</div", $field_content);

					// Convert td elements to div + class
					$field_content = str_replace("<td class='", "<div class='u-table-cell ", $field_content);
					$field_content = str_replace("<td>", "<div class='u-table-cell'>", $field_content);
					$field_content = str_replace("</td", "</div", $field_content);
				}
			}

			return $field_content;
		}, 10, 2 );

	/* === END === */

	/* ==== When the user clicks next or submit, scroll to the top of the form so that they can see any messages/continue the form from the top ==== */

		function gf_scroll_on_continue() { ?>
			<script>
				jQuery(document).on('gform_page_loaded', function(event, form_id, current_page) {
					if( $('gf_page_steps').length > 0 ) {
						$('html, body').animate({
							scrollTop: $(".gf_page_steps").offset().top - 100
						}, 500);
					}
				});
			</script>
		<? }

		function include_scroll( $button, $form ) { 
			// Include script for scrolling on next/prev
			add_action( 'wp_footer', 'gf_scroll_on_continue', 100 );

			// Return submit button
			return $button;
		}
		add_filter( 'gform_submit_button', 'include_scroll', 10, 2 );

	/* === END === */

	/* ==== Improve Gravity Forms validation errors by automatically focusing on the first field with a validation error ==== */

		function gw_first_error_focus( $form ) { ?>
			<script type="text/javascript">
				$(function() {
					$( document ).bind( 'gform_post_render', function() {  

						// Get first error element
						var $firstError = $( 'li.gfield.gfield_error:first' );

						if( $firstError.length > 0 ) {

							// Focus invalid field
							$firstError.find( 'input, select, textarea' ).eq( 0 ).focus();

							// Scroll to first error
							$('body').animate({
								myScrollTop:$firstError.offset().top - 100
							}, {
								duration: 600,
								easing: 'swing',
								step: function(val) {
									window.scrollTo(0, val);
								}
							});
						}
					});
				});
			</script>
			<?
			return $form;
		}
		add_filter( 'wp_footer', 'gw_first_error_focus', 20 );

	/* === END === */
?>

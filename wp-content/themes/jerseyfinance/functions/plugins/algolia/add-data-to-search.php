<?php 
	// Make it index more at a time, making it a little faster
	add_filter( 'algolia_indexing_batch_size', function() {
		return 400;
	} );

	/* ==== Stop certain post types from being indexed ==== */

		function exclude_post_types( $should_index, WP_Post $post ) {
			if ( false === $should_index ) {
				return false;
			}

			// If the post is marked as private, hide it from search
			if( get_field( 'make_private', $post->ID ) ):
				return false;
			endif;			

			// Add all post types you don't want to make searchable.
			$excluded_post_types = array( 'all-timeline-items', 'all-working-groups', 'all-aifmd-countries', 'all-fund-countries' );

			return ! in_array( $post->post_type, $excluded_post_types, true );
		}

		// Hook into Algolia to manipulate the post that should be indexed.
		add_filter( 'algolia_should_index_searchable_post', 'exclude_post_types', 10, 2 );

	/* === END === */

	/* === Fetch text components from the flexible content and add it into the search index === */

	function fetch_flexible_content_text( $post_id ) {
		$flexible_content = '';

		// Fetch all text components, and return the html from both columns
		if( have_rows('fields', $post_id) ):
			while ( have_rows('fields', $post_id) ) : the_row();
				if( get_row_layout() == 'text' ): 

					if( get_sub_field('text') ):
						$flexible_content .= strip_tags( get_sub_field('text') );
					endif;

					if( get_sub_field('first_column_text') ):
						$flexible_content .= strip_tags( get_sub_field('first_column_text') );
					endif;

				endif;
			endwhile;
		endif;

		// The work post type has extra flexible content that needs to be indexed
		if( have_rows('additional_work_tabs', $post_id) ):
			while ( have_rows('additional_work_tabs', $post_id) ) : the_row();
				if( have_rows('fields', $post_id) ):
					while ( have_rows('fields', $post_id) ) : the_row();
						if( get_row_layout() == 'text' ): 

							if( get_sub_field('text') ):
								$flexible_content .= strip_tags( get_sub_field('text') );
							endif;

							if( get_sub_field('first_column_text') ):
								$flexible_content .= strip_tags( get_sub_field('first_column_text') );
							endif;

						endif;
					endwhile;
				endif;
			endwhile;
		endif;

		// If there is a crazy amount of text, trim to 5000 characters (10,000 is Algolias total limit per entry)
		$flexible_content = strlen( $flexible_content ) > 5000 ? substr($flexible_content, 0, 5000) : $flexible_content;

		return $flexible_content;
	}

	/* === Add the text from headers into the search index === */

	function fetch_header_content_text( $post_id ) {
		$header_text = '';

		if( get_field('header_content') ):

			$header_text .= get_field('header_content') ? get_field('header_content')['text'] : '';

		endif;


		return $header_text;
	}

	/* === Fetch the image that will be displayed on our search results === */

	function fetch_search_thumbnail( $post_id ) {
		$results_image = '';

		$post_type = get_post_type( $post_id );

		/* === Depending on the post type, grab different types of image === */

			if ( $post_type == 'all-team' ):

				// Return their profile image
				$image = get_field('profile_image', $post_id);
				$results_image	= !empty($image) ? $image['sizes']['archive-grid-small'] : '';

			elseif ( $post_type == 'all-businesses' ):

				// Return their logo
				$image = get_field('social_logo', $post_id) ? get_field('social_logo', $post_id) : get_field('logo', $post_id);
				
				if( $image ):
					$results_image	= get_field('social_logo', $post_id) ? $image['sizes']['logo-square'] : $image['sizes']['logo-full'];
				endif;

			elseif ( $post_type == 'all-news' || $post_type == 'all-events' ):

				// Fetch the image that has been set for the archive image
				$image = get_field('thumbnail', $post_id);
				$results_image	= !empty($image) ? $image['sizes']['archive-grid-small'] : '';

			elseif( $post_type == 'all-work' ):
				// Fetch the image that has been set for the archive image, but also check for the image on the category in case it isn't set
				$image = get_field('thumbnail', $post_id);
				$results_image	= !empty($image) ? $image['sizes']['archive-grid-small'] : '';

				// If no archive image is set, try to grab a default image from the category
				if( empty($results_image) ): 
					// Fetch the categories for this post
					$categories = get_the_terms($post_id, 'all-work-types');
					
					if( isset($categories) ):
						foreach( $categories as $category ):
							$results_image = get_field('default_thumbnail', $category)['sizes']['archive-grid-small'];
						endforeach;
					endif;
				endif;

			endif;

			// If all of the above has failed to set the results image, default to header image
			if( empty($results_image) ):
				// Return the header image
				$image = get_field('header_image', $post_id);
				$results_image	= !empty( $image ) ? $image['sizes']['archive-grid-small'] : '';
			endif;

		/* === END === */
		
		return $results_image;
	}

	/* === Add custom data for each post type to the search indexes === */

	function add_profile_data_to_search( array $attributes, WP_Post $post ) {
		$post_type = $post->post_type;

		if ( $post_type == 'all-team' ):

			$attributes['job_title'] 	= get_field( 'job_title', $post->ID );
			$attributes['email'] 		= get_field( 'email', $post->ID );
			$attributes['telephone'] 	= get_field( 'telephone', $post->ID );

		elseif( $post_type == 'all-markets' || $post_type == 'all-sectors' ):

			$post_introduction = get_field('post_introduction', $post->ID);
			
			if( $post_introduction ):
	
				$introduction = '';
				$introduction .= $post_introduction['heading'];
				$introduction .= $post_introduction['introduction'];

				$attributes['introduction'] 	= $introduction;
			endif;

		elseif( $post_type == 'all-businesses' ):

			$attributes['overview'] 		= get_field( 'overview', $post->ID );
		
		elseif( $post_type == 'all-events' ):

			$attributes['hide_date'] 		= get_field('hide_date', $post->ID);

			// Get dates from repeater
			$dates = get_field('dates', $post->ID)[0];
			
			$start_date = date_create_from_format('d/m/Y', $dates['start_date']); 
			$end_date 	= date_create_from_format('d/m/Y', $dates['end_date']); 

			$date = $dates['start_date'];
			$date .= $dates['end_date'] && $dates['start_date'] !== $dates['end_date'] ? ' - ' : '';
			$date .= $dates['start_date'] !== $dates['end_date'] ? $dates['end_date'] : '';

			$attributes['event_date'] 	= $date;

		else:
			// Do nothing, just return the default attributes
		endif;

		// Fetch the image to be used in search results
		$attributes['results_image'] = fetch_search_thumbnail( $post->ID );

		// Push text from flexible content into the index
		$attributes['header_content'] = fetch_header_content_text( $post->ID );

		// Push text from flexible content into the index
		$attributes['flexible_content'] = fetch_flexible_content_text( $post->ID );

		// Add our custom keyword field to the search index
		$attributes['search_keywords'] = get_field('search_keywords', $post->ID );

		return $attributes;
	};

	add_filter( 'algolia_post_shared_attributes', 'add_profile_data_to_search', 10, 2 );
	add_filter( 'algolia_searchable_post_shared_attributes', 'add_profile_data_to_search', 10, 2 );
?>
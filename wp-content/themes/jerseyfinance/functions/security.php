<?
	/*
   * This file is just for WordPress security and thats about it
   * Uses send_headers as opposed to wp_header so as to ensure it works even when the webiste is cached
  	*/

  	add_action('send_headers', function() {
		// Remove the powered by header
		header_remove("X-Powered-By");

		// Enforce the use of HTTPS, check every 6 months
		header("Strict-Transport-Security: max-age=15778800; includeSubDomains");

		// Prevent Clickjacking
		header("X-Frame-Options: SAMEORIGIN");

		// Prevent XSS Attack
		header("Content-Security-Policy: default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval' *.gstatic.com *.google.com *.google-analytics.com *.googletagmanager.com *.tagmanager.google.com https://tagmanager.google.com *.fonts.net *.vimeocdn.com data: ; style-src 'self' 'unsafe-inline' *.gstatic.com *.google.com *.google-analytics.com *.googletagmanager.com *.tagmanager.google.com https://tagmanager.google.com *.fonts.net ; font-src 'self' *.fonts.net data: ; frame-src 'self' *.youtube.com *.vimeo.com data: ; "); // Chrome, Firefox etc

		header("X-Content-Security-Policy: default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval' *.gstatic.com *.google.com *.google-analytics.com *.googletagmanager.com *.tagmanager.google.com https://tagmanager.google.com *.fonts.net data: ; style-src 'self' 'unsafe-inline' *.gstatic.com *.google.com *.google-analytics.com *.googletagmanager.com *.tagmanager.google.com https://tagmanager.google.com *.fonts.net ; font-src 'self' *.fonts.net data: ; frame-src 'self' *.youtube.com *.vimeo.com data: ;"); // Chrome, Firefox etc

		// Block Access If XSS Attack Is Suspected
		header("X-XSS-Protection: 1; mode=block");

		// Referrer Policy
		header("Referrer-Policy: same-origin");
  	}, 1);

	// remove wp api stuff from http header
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	remove_action('template_redirect', 'wp_shortlink_header', 11, 0 );
?>
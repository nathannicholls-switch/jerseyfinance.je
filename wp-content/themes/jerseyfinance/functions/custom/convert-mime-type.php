<?
	/* ===== CONVERT MIME TYPE INTO PRETTY EXTENSION ===== */

	function convertMime($mime_type) {

		$extensions = array(
			'image/jpeg' => 'JPG',
			'image/jpg' => 'JPG',
			'application/pdf' => 'PDF',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'doc',
			'XML' => 'text/xml',
			'application/msword' => 'doc',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'doc',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.template' => 'dotx',
			'application/vnd.ms-word.document.macroEnabled.12' => 'docm',
			'application/vnd.ms-word.template.macroEnabled.12' => 'dotm',
			'application/vnd.ms-excel' => 'xls',
			'application/vnd.ms-excel' => 'xlt ',
			'application/vnd.ms-excel' => 'xla ',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xls',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => 'xltx',
			'application/vnd.ms-excel.sheet.macroEnabled.12' => 'xlsm',
			'application/vnd.ms-excel.template.macroEnabled.12' => 'xltm',
			'application/vnd.ms-excel.addin.macroEnabled.12' => 'xlam',
			'application/vnd.ms-excel.sheet.binary.macroEnabled.12' => 'xlsb',
			'application/vnd.ms-powerpoint' => 'ppt',
			'application/vnd.ms-powerpoint' => 'pot ',
			'application/vnd.ms-powerpoint' => 'pps ',
			'application/vnd.ms-powerpoint' => 'ppa ',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'ppt',
			'application/vnd.openxmlformats-officedocument.presentationml.template' => 'potx',
			'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => 'ppsx',
			'application/vnd.ms-powerpoint.addin.macroEnabled.12' => 'ppam',
			'application/vnd.ms-powerpoint.presentation.macroEnabled.12' => 'pptm',
			'application/vnd.ms-powerpoint.template.macroEnabled.12' => 'potm',
			'application/vnd.ms-powerpoint.slideshow.macroEnabled.12' => 'ppsm',
			'application/vnd.ms-access' => 'mdb'
		);

		// Add as many other Mime Types / File Extensions as you like
		if( isset( $extensions[$mime_type] ) ):
			return $extensions[$mime_type];
		else: 
			return $mime_type;
		endif;
	}

	/* === END === */
?>
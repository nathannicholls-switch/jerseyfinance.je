<?
	/* 
		This function allows you to define a specific set of post types to pull shared terms from
		This enables us to fetch terms that are only used on the target post types
		IMPORTANT: Expects arrays
		
		e.g:
		get_terms_by_post_type( array('all-topics'), array('all-news') );
	*/
	function get_terms_by_post_type( $taxonomies, $post_types, $order = 'term_order' ) {

		global $wpdb;

		$query = $wpdb->prepare(
			"SELECT t.*, COUNT(*) from $wpdb->terms AS t
			INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
			INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
			INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
			WHERE p.post_type IN('%s') AND tt.taxonomy IN('%s')
			GROUP BY t.term_id
			ORDER BY $order ASC",
			join( "', '", $post_types ),
			join( "', '", $taxonomies )
		);

		$results = $wpdb->get_results( $query );

		return $results;

	}
?>
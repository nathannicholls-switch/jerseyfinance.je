<?
	/*
		This function will look for any elements that have AMP equivalents
		It will then attempt to replace the tag with the amp version whilst retaining properies likes src, width, height etc.
	*/

	function ampify_html( $html = '' ) {
		
		// These patterns replace the tags, but retains everything inside brackets e.g:
		// `<iframe (src)></iframe>` => `<amp-iframe (src)></amp-iframe>`
		$patterns = array(
			'#<img([^>]+)/>#i', 
			'#<iframe([^>]+)><\/iframe>#i',
			'#<video([^>]+)?>(.+)?</video>#i', // Works for inline src attributes and <source> elements
			'#<audio([^>]+)?>(.+)?</audio>#i', // Works for inline src attributes and <source> elements
		);

		// The replacements for the above
		// $1, $2 are the properties that are in brackets above
		$replacements = array(
			'<amp-img $1 layout="responsive" lightbox></amp-img>',
			'<amp-iframe $1 layout="responsive" sandbox="allow-scripts allow-same-origin"></amp-iframe>',
			'<amp-video $1 layout="responsive" sandbox="allow-scripts allow-same-origin">$2</amp-video>',
			'<amp-audio $1 layout="responsive" sandbox="allow-scripts allow-same-origin">$2</amp-audio>',
		);

		$html = preg_replace(
			$patterns,
			$replacements,
			$html
		);

		// Fix any amp-img tags that use style attribute for width and height
		$html = preg_replace( 
			'#<amp-img(.+)style="width: (.+)px; height: (.+)px;"#i', 
			'<amp-img$1 width="$2" height="$3"', 
			$html 
		);

		// echo htmlentities($html);

		return $html;
	}
?>
<?
	function return_no_posts_message() { ?>

		<div class="container">
			<h4 class="u-align-centre u-margin-top-60 u-margin-bottom-60">
		
				<? 
					if( is_post_type_archive('all-vacancies') ):
						echo "A number of our finance firms who are part of the Jersey Finance CEO Connect programme are able to post their Jersey-based jobs on our website. Unfortunately there are no current vacancies posted. To find out more about jobs in our industry, please contact the finance firms in our <a href=\"/business-directory/\">Members Directory</a>. You can also visit the <a href=\"https://www.gov.je/Working/JobCareerAdvice/Pages/JobsByCategory.aspx?JobTypeID=3\" target=\"_blank\">government website</a> or contact Jersey based recruitment agencies.";
					elseif( is_post_type_archive('all-jfl-vacancies') ):
						echo "Sorry, all of our roles are currently filled but please watch this space for updates, as opportunities do arise from time to time";
					elseif( is_post_type_archive('all-businesses') ):
						echo "Sorry, there are no Jersey businesses in our directory matching your description";
					elseif( is_page('manage-posts') ):
						echo "You currently have no posts to manage. Once you join a company that has published content to the Jersey Finance website you will be able to manage their content here";
					elseif( is_page('manage-businesses') ):
						echo "You currently have no businesses to manage";
					else:
						echo "Sorry, no posts were found";
					endif;
				?>
			</h4>
		</div>
	<? }
?>
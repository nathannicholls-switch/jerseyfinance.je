<?
	/* ==== Shorten and return the characters to a user specified amount OR default to 230 ==== */

		function shorten_text($text = "", $length = 230, $tags = true) {
			$text = strip_tags($text);

			if( strlen($text) > $length ):
				$text = substr($text, 0, $length) . '...';
			endif;

         if( $tags ):
            $text = '<p>' . $text . '</p>';
         endif;

			return $text;
		}

	/* ==== END ==== */
?>
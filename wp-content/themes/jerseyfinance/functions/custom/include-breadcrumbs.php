<?
	function include_breadcrumbs( $color = 'red' ) {
		global $post; 

		// We use the breaadcrumb for spacing, so if this is a parent page just output empty breadcrumbs
		if ( function_exists('yoast_breadcrumb') ) {
			if( is_page() && $post->post_parent == 0 ):
				echo '<div class="c-breadcrumbs">&nbsp;</div>';
			else:
				yoast_breadcrumb( '<div class="c-breadcrumbs ' . $color . '">','</div>' );
			endif;			
		}
	}

	function adjust_breadcrumb( $link_output ) {
		global $post;

		/* ==== Remove post title from pages and singles ==== */

			if( ( is_page() && $post->post_parent !== 0 ) || is_singular() ) {
				if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {

					// Remove the last breadcrumb & greater than symbol
					$link_output = preg_replace( '~&gt; <span class="breadcrumb_last">(.*?)</span>~', '', $link_output );
				}
			}

		/* === END === */

		/* ==== On Market templates include the parent page in the breadcrumbs as Yoast can't detect this from fan archive ==== */

			if( strpos( $link_output, '>Markets<' ) !== false || strpos( $link_output, 'Sectors' ) !== false ) {
				
				$link_output = preg_replace( '~Home</a>~', 'Home</a> &gt; <a href="/jersey-the-finance-centre">Jersey the Finance Centre</a>', $link_output );
				$link_output = preg_replace( '~&gt; <span class="breadcrumb_last">(.*?)</span>~', '', $link_output );
			}

		/* === END === */

		/* ==== News - include "Type" in breadcrumbs and link it to a filtered search results page ==== */

			if( is_singular('all-news') ):

				// get first "Type" terms
				$type_term = get_the_terms(get_the_ID(), 'all-news-types')[0];
				
				if( $type_term ):
					// Generate a link to the archive with this category selected
					$archive_url = get_post_type_archive_link( get_post_type() ) . '?news-types[]=' . $type_term->term_id;

					// Generate breadcrumb link
					$type =  ' &gt; <span><a href="' . $archive_url . '">' . $type_term->name . '</a> </span>';
				else:
					$type = '';
				endif;
				
				// Output after last item
				$link_output = '<div class="c-breadcrumbs red"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span><a href="/news/">News</a> ' . $type . ' </span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */		

		/* ==== Work - include "Type" in breadcrumbs and link it to a filtered search results page ==== */

			if( is_singular('all-work') ):

				// get first "Type" terms
				$type_term = get_the_terms(get_the_ID(), 'all-work-types')[0];
				
				if( $type_term ):
					// Generate a link to the archive with this category selected
					$archive_url = get_post_type_archive_link( get_post_type() ) . '?work-types[]=' . $type_term->term_id;

					// Generate breadcrumb link
					$type =  ' &gt; <span><a href="' . $archive_url . '">' . $type_term->name . '</a> </span>';
				else:
					$type = '';
				endif;
				
				// Output after last item
				$link_output = '<div class="c-breadcrumbs red"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span><a href="/our-work/">Work</a> ' . $type . ' </span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */		

		/* ==== News - include "Type" in breadcrumbs and link it to a filtered search results page ==== */

			if( is_singular('all-working-groups') ):				
				$link_output = '<div class="c-breadcrumbs red"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span>Working Groups</span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */

		/* ==== The Team and The Board pages ==== */

			if( is_tax('all-team-groups') ):
				$link_output = '<div class="c-breadcrumbs red"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span><a href="/who-we-are/">Who We Are</a> </span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */

		/* ==== Spotlight on hubs ==== */

			if( is_tax('all-topics') ):
				// First group
				$term = get_queried_object();

				$term_link = $term ? '&gt; <span><a href="' . get_term_link($term, 'all-team-groups') . '">' . $term->name . '</a>' : '';

				$link_output = '<div class="c-breadcrumbs white"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span><a href="/our-work/?work-types%5B%5D=88">Spotlight On</a> ' . $term_link . ' </span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */

		/* ==== Business Categories ==== */

			if( is_tax('all-business-categories') ):

				// First group
				$term = get_queried_object();

				$term_link = $term ? '&gt; <span><a href="' . get_term_link($term, 'all-business-categories') . '">' . $term->name . '</a>' : '';

				$link_output = '<div class="c-breadcrumbs red"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span><a href="/business-directory/">Business Directory</a> ' . $term_link . ' </span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */

		/* ==== Individual profiles ==== */

			if( is_singular('all-team') ):
				// First group
				$term = get_the_terms(get_the_ID(), 'all-team-groups')[0];

				$term_link = $term ? '&gt; <span><a href="' . get_term_link($term, 'all-team-groups') . '">' . $term->name . '</a>' : '';

				$link_output = '<div class="c-breadcrumbs red"><span><span>';
					$link_output .= '<a href="/">Home</a> &gt; <span><a href="/who-we-are/">Who We Are</a> ' . $term_link . ' </span>';
				$link_output .= '</span></span></div>';
			endif;

		/* === END === */

		return $link_output;
	}
	add_filter('wpseo_breadcrumb_output', 'adjust_breadcrumb' );

?>
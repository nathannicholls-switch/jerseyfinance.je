<?
   /*
      Summary:
         - Enqueue custom stylesheets
         - Enqueue custom scripts
	*/

	/* ==== Custom stylesheets ==== */

		function enqueue_stylesheets() {
			wp_enqueue_style( 'font_stylesheet', '//fast.fonts.net/cssapi/cdd45045-3e98-480a-97ce-0f1aae95dc86.css', false );

			wp_enqueue_style( 'theme_stylesheet', get_template_directory_uri() . '/dist/css/style.css' , false, '1.21.89' );
		}
		add_action('wp_enqueue_scripts', 'enqueue_stylesheets', 10, 1);
	
	/* === END === */

	/* ==== Custom scripts ==== */

		function enqueue_scripts() {
			wp_enqueue_script( 'jquery', get_template_directory_uri() . '/dist/js/main.js' , array(), '1.37', true );
		}
		add_action('wp_enqueue_scripts', 'enqueue_scripts', 10, 1);

		// Have to do it slightly differently on the login page because *Wordpress*
		function enqueue_login_scripts() {
				wp_enqueue_script( 'my-js', get_template_directory_uri() . '/dist/js/main.js', false );
		}
		add_action('login_enqueue_scripts', 'enqueue_login_scripts', 10, 1);
	
	/* === END === */
?>
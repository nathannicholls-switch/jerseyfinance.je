<?
   /*
      Summary:
         - Enable basic Wordpress features:
				- Appearance > Menus
				- Title tags
			- If robots.txt is set to no-index, put banner on Wordpress admin bar
			- Set default icon options across all icon fields
	*/

	/* ==== Enable Features ==== */

		register_nav_menus();               // Appearance > Menus
		add_theme_support( 'title-tag' );   // Title tags

	/* ==== END ==== */

	/* ==== Remove WP automatic redirects ==== */

		remove_action('template_redirect', 'redirect_canonical');
		
	/* === END === */

	/* ==== If robots.txt is set to no-index, display warning banner on Wordpress bar ==== */

	 	function check_site_robots() {
			$blog_public = get_option( 'blog_public' );
			
			if( !$blog_public ) {
				echo '<style>#wpadminbar:after {background: rgb(0,0,0);background: -moz-linear-gradient(-45deg, rgba(35,40,45,1) 35%, rgba(252,222,0,1) 35%, rgba(252,222,0,1) 65%, rgba(35,40,45,1) 65%);background: -webkit-linear-gradient(-45deg,  rgba(35,40,45,1) 35%,rgba(252,222,0,1) 35%,rgba(252,222,0,1) 65%,rgba(35,40,45,1) 65%);background: linear-gradient(135deg,  rgba(35,40,45,1) 35%,rgba(252,222,0,1) 35%,rgba(252,222,0,1) 65%,rgba(35,40,45,1) 65%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#23282d", endColorstr="#23282d",GradientType=1 );background-size: 18px 5px;content: "";display: block;position: absolute;top: 100%;left: 0;height: 5px;width: 100%;}</style>';
			}
		}
		add_action('admin_head', 'check_site_robots');

	/* ==== END ==== */

	/* ==== Set default icon options across all icon fields ==== */

		function acf_load_icon_field_choices( $field ) {
			
			// Reset choices
			$field['choices'] = array();			

			$choices = array(
				'aeroplane' 			=> 'Aeroplane',
				'bar' 					=> 'Bar',
				'book' 					=> 'Book',
				'books' 					=> 'Books',
				'brexit' 				=> 'Brexit',
				'briefcase' 			=> 'Briefcase',
				'btc' 					=> 'Btc',
				'calculator' 			=> 'Calculator',
				'calendar' 				=> 'Calendar',
				'chart' 					=> 'Chart',
				'check' 					=> 'Check',
				'clock' 					=> 'Clock',
				'cogs' 					=> 'Cogs',
				'city' 					=> 'City',
				'compass' 				=> 'Compass',
				'digital' 				=> 'Digital',
				'discussions' 			=> 'Discussions',
				'document' 				=> 'Document',
				'dollar' 				=> 'Dollar',
				'down-alt' 				=> 'Down Alt',
				'down' 					=> 'Down',
				'euro' 					=> 'Euro',
				'europe' 				=> 'Europe',
				'exclamation' 			=> 'Exclamation',
				'eye' 					=> 'Eye',
				'female' 				=> 'Female',
				'fifty' 					=> 'Fifty Years',
				'flag-cn' 				=> 'Flag- China',
				'flag-kn' 				=> 'Flag- Kenya',
				'flag-sa' 				=> 'Flag- South Africa',
				'flag-tri' 				=> 'Flag- Tricolour',
				'flag-uk' 				=> 'Flag- UK',
				'flag-us' 				=> 'Flag- US',
				'fingerprint' 			=> 'Fingerprint',
				'globe' 					=> 'Globe',
				'government' 			=> 'Government',
				'gravel' 				=> 'Gravel',
				'group' 					=> 'Group',
				'growth' 				=> 'Growth',
				'handshake' 			=> 'Handshake',
				'heart' 					=> 'Heart',
				'info' 					=> 'Info',
				'jersey' 				=> 'Jersey',
				'laptop' 				=> 'Laptop',
				'leaves' 				=> 'Leaves',
				'left-alt' 				=> 'Left Alt',
				'left' 					=> 'Left',
				'lightbulb' 			=> 'Lightbulb',
				'lighthouse' 			=> 'Lighthouse',
				'magnify' 				=> 'Magnify',
				'male' 					=> 'Male',
				'mobile' 				=> 'Mobile',
				'mouse' 					=> 'Mouse',
				'pen' 					=> 'Pen',
				'pie' 					=> 'Pie',
				'pound' 					=> 'Pound',
				'question' 				=> 'Question',
				'research' 				=> 'Research',
				'right-alt' 			=> 'Right Alt',
				'right' 					=> 'Right',
				'rosette' 				=> 'Rosette',
				'scales' 				=> 'Scales',
				'scroll' 				=> 'Scroll',
				'seal' 					=> 'Seal',
				'shield' 				=> 'Shield',
				'smartphone' 			=> 'Smartphone',
				'star' 					=> 'Star',
				'stopwatch' 			=> 'Stopwatch',
				'strategy' 				=> 'Strategy',
				'trophy' 				=> 'Trophy',
				'up-alt' 				=> 'Up Alt',
				'up' 						=> 'Up',
				'video' 					=> 'Video',
				'voting' 				=> 'Voting',
				'workforce-female'	=> 'Workforce Female',
				'workforce-male'		=> 'Workforce Male',
			);
			
			// Loop through array and add to field 'choices'
			if( is_array($choices) ) {
				foreach( $choices as $key => $choice ) {
					$field['choices'][ $key ] = $choice;
				}
			}
			
			// Return the field
			return $field;			
		}

		add_filter('acf/load_field/name=icon', 'acf_load_icon_field_choices');

	/* ==== END ==== */

	function wpa_show_permalinks( $post_link, $post ){
		if ( is_object( $post ) && $post->post_type == 'all-team' ){
			$terms = wp_get_object_terms( $post->ID, 'all-team-groups' );
			if( $terms ){
				return str_replace( '%all-team-groups%' , $terms[0]->slug , $post_link );
			}
		}
		return $post_link;
	}
	add_filter( 'post_type_link', 'wpa_show_permalinks', 1, 2 );

	/* ==== Add custom styles to editor ===== */
		function add_style_select_buttons( $buttons ) {
			array_unshift( $buttons, 'styleselect' );
			return $buttons;
		}

		add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

		function add_styles_to_editor( $init_array ) {  
			// Define the style_formats array
			$style_formats = array(  
				// Each array child is a format with it's own settings
				array(  
					'title' => 'h1 (class)',
					'block' => 'span',
					'classes' => 'h1',
					'wrapper' => false,
				),
				array(  
					'title' => 'h2 (class)',
					'block' => 'span',
					'classes' => 'h2',
					'wrapper' => false,
				),
				array(  
					'title' => 'h3 (class)',
					'block' => 'span',
					'classes' => 'h3',
					'wrapper' => false,
				),
				array(  
					'title' => 'h4 (class)',
					'block' => 'span',
					'classes' => 'h4',
					'wrapper' => false,
				),
				array(  
					'title' => 'h5 (class)',
					'block' => 'span',
					'classes' => 'h5',
					'wrapper' => false,
				),
				array(  
					'title' => 'h6 (class)',
					'block' => 'span',
					'classes' => 'h6',
					'wrapper' => false,
				),
				array(  
					'title' => 'Paragraph Small (sans)',
					'selector' => 'p',
					'classes' => 'u-font-size-small',
				),
				array(  
					'title' => 'Paragraph Large (serif)',
					'selector' => 'p',
					'classes' => 'u-font-size-large',
				),
				array(  
					'title' => 'Button',
					'selector' => 'a',
					'classes' => 'c-button red',
				),
				array(  
					'title' => 'Reduced List Height',
					'selector' => 'ul,ol',
					'classes' => 'u-reduced-list-height',
				),
			);  
			// Insert the array, JSON ENCODED, into 'style_formats'
			$init_array['style_formats'] = json_encode( $style_formats );  
			
			return $init_array;  
		} 
		add_filter( 'tiny_mce_before_init', 'add_styles_to_editor' );  

	/* === END === */

	// Stop WordPress from modifying .htaccess permalink rules
	add_filter('flush_rewrite_rules_hard','__return_false');
?>
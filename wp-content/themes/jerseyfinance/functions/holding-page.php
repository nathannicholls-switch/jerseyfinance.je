<?
	function enable_holding_page() {

		$frontpage_id = get_option( 'page_on_front' );

		$page_template = get_page_template_slug($frontpage_id);

		// If the homepage has the holding page template enabled
		if( $page_template == 'page-holding-page.php' ):

			// If they are trying to look at anything except the homepage, redirct to home
			if ( !is_user_logged_in() && !is_front_page() && $_SERVER['PHP_SELF'] != '/wp-admin/admin-ajax.php' ):
				wp_redirect( site_url() ); 
				exit();
			endif;
		endif;
	}

	add_action( 'template_redirect', 'enable_holding_page' );
?>
<?

	// Filter that allows us to target a repeater field with `meta_query`
	function my_posts_where( $where ) {
		$where = str_replace("meta_key = 'domain_names_$", "meta_key LIKE 'domain_names_%", $where);
		return $where;
	}
	add_filter('posts_where', 'my_posts_where');

	// Return an array of businesses that the user is allowed to manage ($unassigned)
	function get_matching_businesses( $user_email, $unassigned = false ) {

		if( $user_email ) {

			$meta_query = [];

			// Get everything after the @
			$user_domain = explode('@', $user_email)[1];

			// Search domain_names repeater for the users domain
			$meta_query[] = array(
				'relation'	=> 'OR',
				array(
					'key' 		=> 'domain_names_$_domain_name',
					'value'		=> "" . $user_domain . "",
					'compare'	=> '='
				)
			);

			// If $unassigned is true, we will only fetch businesses that don't currently have the user in the user field 
			// (useful for shwoing which busineses users can opt into managing if their account was created before the business)
			if( $unassigned ):
				$meta_query['relation'] = 'AND';

				// Check if the user was already logged in or if a new user was created and grab their ID.
				if( get_current_user_id() ) {
					$user_ID = get_current_user_id();
				} else {
					$user_ID = get_user_by( 'email', $entry[$form_fields['user']['email']['entry_key']] )->ID;
				}

				$meta_query[] = array(
					'key' 		=> 'owner',
					'value'		=> '"' . $user_ID . '"',
					'compare'	=> 'NOT LIKE'
				);

				$meta_query[] = array(
					'key' 		=> 'users',
					'value'		=> '"' . $user_ID . '"',
					'compare'	=> 'NOT LIKE'
				);

				$meta_query[] = array(
					'key' 		=> 'additional_users',
					'value'		=> '"' . $user_ID . '"',
					'compare'	=> 'NOT LIKE'
				);
				
			endif;

			// Get all matching businesses
			$args = array(
				'post_type' 		=> 'all-businesses',
				'posts_per_page'	=> -1,
				'meta_query'		=> $meta_query
			);

			$eligible_businesses = new WP_Query( $args );

			$businesses = array();

			if( $eligible_businesses->have_posts() ):
				while( $eligible_businesses->have_posts() ): $eligible_businesses->the_post();

					// Push business name and ID into an array, encrypt ID with md5 so that if we use on front end people can't mess with it in the source
					// (if we just used ID then the user could simply increment/guess it to gain access to another business)
					$businesses[get_the_title()] = get_the_ID();

				endwhile;
			endif;

			// Return the array of business names and ID's
			return $businesses;
		}
	}

	// Get business that have added this user to their user fields
	// (useful if people have been added to businesses by JFL team, as technically they might not have a matching domain)
	function get_joined_businesses( $user_ID, $owner_only = false ) {

		if( $user_ID ):

			$meta_query['relation'] = 'OR';

			$meta_query[] = array(
				'key' 		=> 'owner',
				'value'		=> '"' . $user_ID . '"',
				'compare'	=> 'LIKE'
			);

			if( !$owner_only ):

				$meta_query[] = array(
					'key' 		=> 'users',
					'value'		=> '"' . $user_ID . '"',
					'compare'	=> 'LIKE'
				);

				$meta_query[] = array(
					'key' 		=> 'additional_users',
					'value'		=> '"' . $user_ID . '"',
					'compare'	=> 'LIKE'
				);
			
			endif;

			// Get all matching businesses
			$args = array(
				'post_type' 		=> 'all-businesses',
				'posts_per_page'	=> -1,
				'meta_query'		=> $meta_query
			);

			$eligible_businesses = new WP_Query( $args );

			$businesses = array();

			if( $eligible_businesses->have_posts() ):
				while( $eligible_businesses->have_posts() ): $eligible_businesses->the_post();

					// Push business name and ID into an array, encrypt ID with md5 so that if we use on front end people can't mess with it in the source
					// (if we just used ID then the user could simply increment/guess it to gain access to another business)
					$businesses[get_the_title()] = get_the_ID();

				endwhile;
			endif;

			// Return the array of business names and ID's
			return $businesses;
		endif;
	}
?>
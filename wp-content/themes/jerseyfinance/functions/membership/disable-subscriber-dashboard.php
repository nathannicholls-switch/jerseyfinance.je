<?
	// Disasble admin bar for anyone who isn't an admin
	if ( !current_user_can('administrator') && !current_user_can('jfl_administrator') && !current_user_can('editor') ) {
		add_filter('show_admin_bar', '__return_false');
	}

	// Don't allow dashboard access for anyone who isn't an admin
	function disable_dashboard() {
		if ( (!current_user_can('administrator') && !current_user_can('jfl_administrator') && !current_user_can('editor') ) && is_admin() && !wp_doing_ajax() ) {
			wp_redirect(home_url());
			exit;
		}
	}
	add_action('admin_init', 'disable_dashboard');
?>
<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$business_update_fields = array(
		'company_name' => array(
			'input_id' 		=> '1',
			'entry_key'		=> '1'
		),
		'introduction' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
			'acf_key'		=> 'field_5bfeb0ad889c1'
		),
		'overview' => array(
			'input_id' 		=> '3',
			'entry_key'		=> '3',
			'acf_key'		=> 'field_5bfeb16477c6d'
		),
		'current_logos' => array(
			'input_id' 		=> '18',
			'entry_key'		=> '18',
		),
		'remove_logo' => array(
			'input_id' 		=> '20',
			'entry_key'		=> '20',
		),
		'remove_social_logo' => array(
			'input_id' 		=> '21',
			'entry_key'		=> '21',
		),
		'logo' => array(
			'input_id' 		=> '4',
			'entry_key'		=> '4',
			'acf_key'		=> 'field_5bf80bdc4ca77',
			'type'			=> 'image'
		),
		'social_logo' => array(
			'input_id' 		=> '5',
			'entry_key'		=> '5',
			'acf_key'		=> 'field_5bf80c074ca78',
			'type'			=> 'image'
		),
		'domain_names' => array(
			'input_id' 		=> '6',
			'entry_key'		=> '6',
			'acf_key'		=> 'field_5bf80c294ca79',
			'type'			=> 'list',

			'columns'		=> array(
				'domain_name' => array(
					'input_id' 		=> '6_1',
					'entry_key'		=> '6.1',
					'acf_key'		=> 'field_5bf80c324ca7a'
				)
			)
		),
		'website' => array(
			'input_id' 		=> '7',
			'entry_key'		=> '7',
			'acf_key'		=> 'field_5bf80c624ca7b'
		),
		'email' => array(
			'input_id' 		=> '8',
			'entry_key'		=> '8',
			'acf_key'		=> 'field_5bf80c8c4ca7c'
		),
		'phone' => array(
			'input_id' 		=> '9',
			'entry_key'		=> '9',
			'acf_key'		=> 'field_5bf80ca14ca7d'
		),
		'address' => array(
			'input_id' 		=> '10',
			'entry_key'		=> '10',
			'acf_key'		=> 'field_5bf80cbb4ca7e'
		),
		'key_contacts' => array(
			'input_id' 		=> '11',
			'entry_key'		=> '11',
			'acf_key'		=> 'field_5bf80cc84ca7f',
			'type'			=> 'list',
			
			// Column names need to match Gravity Forms labels, and be in the same order as ACF
			'columns'		=> array(
				'Name' => array(
					'input_id' 		=> '11_1',
					'entry_key'		=> '11.1',
					'acf_key'		=> 'field_5bfd2242e2dd2'
				),
				'Job Title' => array(
					'input_id' 		=> '11_2',
					'entry_key'		=> '11.2',
					'acf_key'		=> 'field_5bfd2251e2dd3'
				),
				'Email' => array(
					'input_id' 		=> '11_3',
					'entry_key'		=> '11,3',
					'acf_key'		=> 'field_5bfd2255e2dd4'
				),
				'Direct Line' => array(
					'input_id' 		=> '11_4',
					'entry_key'		=> '11.4',
					'acf_key'		=> 'field_5bfd2272e2dd5'
				),
				'Image' => array(
					'type'			=> 'image',
					'input_id' 		=> '11_5',
					'entry_key'		=> '11.5',
					'acf_key'		=> 'field_5bfd227ae2dd6'
				)
			)
		),
		'twitter' => array(
			'input_id' 		=> '12',
			'entry_key'		=> '12',
			'acf_key'		=> 'field_5bf80ce74ca80'
		),
		'linkedin' => array(
			'input_id' 		=> '13',
			'entry_key'		=> '13',
			'acf_key'		=> 'field_5bf80d174ca82'
		),
		'facebook' => array(
			'input_id' 		=> '14',
			'entry_key'		=> '14',
			'acf_key'		=> 'field_5bf80d164ca81'
		),
		'related_sectors' => array(
			'input_id' 		=> '15',
			'entry_key'		=> '15',
			'acf_key'		=> 'field_5bf80d304ca83',
			'type'			=> 'checkbox',
		),
		'related_markets' => array(
			'input_id' 		=> '16',
			'entry_key'		=> '16',
			'acf_key'		=> 'field_5bf80d4b4ca84',
			'type'			=> 'checkbox',
		)
	);
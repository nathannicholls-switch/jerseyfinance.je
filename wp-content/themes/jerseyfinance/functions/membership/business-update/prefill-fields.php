<?
	/* ==== Business Name ==== */

	   function prefill_business_name($value) {
			global $business_update_fields;
			global $edit_post_id;

			return get_the_title( $edit_post_id );
	   }
	   add_filter("gform_field_value_business_name", "prefill_business_name");

	/* ==== Business Intro ==== */

	   function prefill_business_introduction($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['introduction']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_introduction", "prefill_business_introduction");

	/* ==== Business Overview ==== */

	   function prefill_business_overview($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['overview']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_overview", "prefill_business_overview");


	/* ==== Business Domains ==== */

	   function prefill_business_domains($value) {
			global $business_update_fields;
			global $edit_post_id;

			return prefillGravityFormsList( $edit_post_id, $business_update_fields['domain_names']['acf_key'], $business_update_fields['domain_names']['columns'] );
	   }
	   add_filter("gform_field_value_business_domains", "prefill_business_domains");


	/* ==== Business Website ==== */

	   function prefill_business_website($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['website']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_website", "prefill_business_website");


	/* ==== Business Email ==== */

	   function prefill_business_email($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['email']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_email", "prefill_business_email");


	/* ==== Business Telephone ==== */

	   function prefill_business_telephone($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['phone']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_telephone", "prefill_business_telephone");


	/* ==== Business Address ==== */

	   function prefill_business_address($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['address']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_address", "prefill_business_address");


	/* ==== Business Key Contacts ==== */

	function prefill_key_contacts($value){
		global $business_update_fields;
		global $edit_post_id;

		/*
			Add a blank row to the bottom - we will hide it and use that when adding rows 
			(otherwise we run into an issue where new rows keep the image from the previous row)
		*/
		$key_contacts = prefillGravityFormsList( $edit_post_id, $business_update_fields['key_contacts']['acf_key'], $business_update_fields['key_contacts']['columns'] );

		// if there are none, make sure two rows output (as we hide the last one)
		if( empty( $key_contacts ) ):
			$key_contacts[] = array();
		endif;

		$key_contacts[] = array();
		
		return $key_contacts;
	}
	add_filter("gform_field_value_key_contacts", "prefill_key_contacts");

	/* ==== Business Sectors ==== */

	   function prefill_business_sectors($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsCheckboxes( $edit_post_id, $business_update_fields['related_sectors']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_sectors", "prefill_business_sectors");


	/* ==== Business Markets ==== */

	   function prefill_business_markets($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsCheckboxes( $edit_post_id, $business_update_fields['related_markets']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_markets", "prefill_business_markets");


	/* ==== Business Twitter ==== */

	   function prefill_business_twitter($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['twitter']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_twitter", "prefill_business_twitter");


	/* ==== Business LinkedIn ==== */

	   function prefill_business_linkedin($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['linkedin']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_linkedin", "prefill_business_linkedin");
	

	/* ==== Business Facebook ==== */

	   function prefill_business_facebook($value) {
			global $business_update_fields;
			global $edit_post_id;
				
			return prefillGravityFormsBasicField( $edit_post_id, $business_update_fields['facebook']['acf_key'] );
	   }
	   add_filter("gform_field_value_business_facebook", "prefill_business_facebook");
?>
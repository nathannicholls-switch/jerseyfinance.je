<?
	function update_business_information($value) {
		global $business_update_fields;
		global $edit_post_id;

		foreach( $business_update_fields as $key => $field):
			//	Regular fields
			if( isset($field['acf_key']) ):
				if( !isset($field['type']) ):
					updateGravityFormsBasicField( $edit_post_id, 'input_' . $field['input_id'], $field['acf_key'] );

				// If lists
				elseif( $field['type'] == 'list' && isset($field['columns']) ):

					updateGravityFormsList( $edit_post_id, 'input_' . $field['input_id'], $field['acf_key'], $field['columns'] );

				endif;
			endif;
			
		endforeach;

		//die();
	}
	add_action( 'gform_pre_submission_12', 'update_business_information', 10, 2 );



	// Function that fires once submitted - removes current logos or takes new logos and replaces current ones
	function update_logos( $entry, $form ) {
		global $business_update_fields;
		global $edit_post_id;

		// echo '<pre>';
		// print_r($entry);
		// echo '</pre>';

		// echo $edit_post_id;

		/* ==== Do we just want to remove the existing logos? ==== */

			if( !empty($entry[$business_update_fields['remove_logo']['entry_key']]) ) {
				update_field( $business_update_fields['logo']['acf_key'], '', $edit_post_id );
			}

			if( !empty($entry[$business_update_fields['remove_social_logo']['entry_key']]) ) {
				update_field( $business_update_fields['social_logo']['acf_key'], '', $edit_post_id );
			}

		/* === END === */

		/* ==== Are there new logos? ==== */

			$logo 			= $entry[$business_update_fields['logo']['entry_key']];
			$social_logo	= $entry[$business_update_fields['social_logo']['entry_key']];

			if( !empty($logo) ):
				$image_id = attachment_url_to_postid( $logo );

				if( $image_id ):
					update_field( $business_update_fields['logo']['acf_key'], $image_id, $edit_post_id );
				endif;
			endif;

			if( !empty($social_logo) ):
				$image_id = attachment_url_to_postid( $social_logo );
				
				if( $image_id ):
					update_field( $business_update_fields['social_logo']['acf_key'], $image_id, $edit_post_id );
				endif;
			endif;
		
		/* === END === */

		/* ==== Update sectors and markets ==== */

			$new_sectors = array();

			// Look for all of the posted values using the forms' field key
			foreach($entry as $key => $value) {
				
				// If the posted key has matches '15.XXX' in it, push the value into the array
				if (strpos($key, $business_update_fields['related_sectors']['entry_key'] . '.') === 0) {
					array_push($new_sectors, $value);
				}
			}

			$new_markets = array();

			// Look for all of the posted values using the forms' field key
			foreach($entry as $key => $value) {
				
				// If the posted key has matches '15.XXX' in it, push the value into the array
				if (strpos($key, $business_update_fields['related_markets']['entry_key'] . '.') === 0) {
					array_push($new_markets, $value);
				}
			}

			if( !empty($new_sectors) ):
				update_field( $business_update_fields['related_sectors']['acf_key'], $new_sectors, $edit_post_id);
			endif;

			if( !empty($new_markets) ):
				update_field( $business_update_fields['related_markets']['acf_key'], $new_markets, $edit_post_id);
			endif;

		/* === END === */
	}
	add_action( 'gform_after_submission_12', 'update_logos', 10, 2 );
?>
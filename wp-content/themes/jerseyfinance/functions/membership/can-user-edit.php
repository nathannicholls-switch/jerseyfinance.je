<?
	/*
		Function used to determine whether or not the user is allowed to edit the selected post.
	*/
	
	function user_can_edit( $post_id ) {

		// If the user is logged in, check if they can edit the post
		if( is_user_logged_in() ):
			// The current user ID
			$current_user_id = get_current_user_id();

			// To make sure they can only edit live/pending posts
			$post_status = get_post_status( $post_id ); 

			// The business ID that published the post - which we check for users against
			if( get_post_type( $post_id ) == 'all-businesses' ):
				$published_by = $post_id;
			else:
				$published_by = get_post_field( 'business', $post_id );
			endif;

			// Get the users from the business that authored the post
			$allowed_users = get_field( 'owner', $published_by ) ? array_column(get_field( 'owner', $published_by ), 'ID') : array(); // an array containing the owners

			if( isset($allowed_users) ):
				// Check whether or not the user exists in the user fields of the business
				if( in_array( $current_user_id, $allowed_users ) ):
					$user_has_permission = true;
				else:
					$user_has_permission = false;
				endif;

				// User is allowed to edit if the post is published and they are part of the business that published the post
				if( $post_status == 'publish' && $user_has_permission ) :
					// User is allowed to edit
					return true;
				else:
					// User cannot edit
					return false; 					
				endif;
			else:
				// No users are tagges, so user cannot edit
				return false; 
			endif;
		else:
			
			return false; // User is not logged in, therefore cannot edit
			
		endif;
	}
?>
<?
	// Manipulate the submitted Gravity Forms data so that it fits in ACF
	function after_timeline_item_creation( $entry, $form ) {
		global $timeline_2018_fields;

		// Make the date ACF friendly
			$submitted_date = $entry[$timeline_2018_fields['date']['entry_key']];
			$submitted_date = DateTime::createFromFormat('d/m/Y', $submitted_date);
			$date = $submitted_date->format('Ymd');

			update_field( $timeline_2018_fields['date']['acf_key'], $date, $entry['post_id'] );	
		// END

		// Map link to link field
			$link = array(
				'title'	=> 'Read Article',
				'url'		=> $entry[$timeline_2018_fields['link']['entry_key']],
			);

			update_field( $timeline_2018_fields['link']['acf_key'], $link, $entry['post_id'] );	
		// END
	}
	add_action( 'gform_after_submission_15', 'after_timeline_item_creation', 10, 2 );		


	function submit_timeline_loaded() {
		global $timeline_2018_fields;

			// Start Date
			$filter_name = 'gform_field_input_15_' . $timeline_2018_fields['date']['input_id'];
			add_filter( $filter_name, 'make_datepicker', 10, 6 );
	}

	add_action( 'init', 'submit_timeline_loaded', 10, 2 );
?>
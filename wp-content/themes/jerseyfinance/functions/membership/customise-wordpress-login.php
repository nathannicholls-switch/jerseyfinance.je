<?
	// Remove login styling
		function remove_login_styles() {
			if( !is_admin() ):
				wp_dequeue_style( 'login' );
			endif;
		}
		add_action('login_enqueue_scripts','remove_login_styles');

	// Include header
		function include_login_header() {
			include( locate_template('/includes/head.php') );

			$pagename = isset($_REQUEST['action']) && $_REQUEST['action'] === 'lostpassword' ? 'forgotten-password' : 'login';

			// Customise the title based on which action the user has taken
			if( $_REQUEST['action'] === 'rp' ):
				$title 	= 'Reset your password';
				$text 	= 'Choose your new password below.';
			endif;

			if( $_REQUEST['action'] === 'resetpass' ):
				$title 	= 'Password successfully updated';
				$text 	= 'Please log in below.';
			endif;

			// Get header content from either the forgotten password page or the login page
			$args = array(
				'pagename' => $pagename
			);

			$page_query = new WP_Query($args);

			if( $page_query->have_posts() ):
				while( $page_query->have_posts() ): $page_query->the_post(); ?>

					<? 
						// Include header (with custom text if set)
						get_template_partial( '/includes/post-headers/one-column-header', array(
							'title' 	=> $title,
							'text'	=> $text
						)); 
					?>

				<? endwhile;
			endif; ?>

			<div class="main-content u-padding-top-20">
				<div class="site-wrapper">
					<div class="u-pattern-wrapper">
						<div class="container">
							<div class="custom-login u-box-shadow-light gform_wrapper">
								<div class="custom-login__form">

								<? 
									// If the user has just updated their password, show success message and login form.
									if( $_REQUEST['action'] === 'resetpass' ):
										$args = array(
											'redirect' => '/dashboard',
										); 
										
										wp_login_form( $args );
									endif
								?>

		<? }
		add_action( 'login_headerurl', 'include_login_header' );

	// Include footer
		function include_login_footer() { ?>

									<p class="login-links">
										<a href="/membership/join">
											Register
										</a>

										<? if ( !isset($_REQUEST['action']) || ( $_REQUEST['action'] !== 'lostpassword' && $_REQUEST['action'] !== 'rp') ): ?>
											|
											<a href="/login?action=lostpassword">
												Lost your password?
											</a>
										<? endif; ?>

										<? if ( isset($_REQUEST['action']) ): ?>
											|
											<a href="/login/">
												Login
											</a>
										<? endif; ?>
									</p>

									<? /*

									<div class="c-title">Or via LinkedIn</div>
									<?
										// Social login plugin
										do_action( 'wordpress_social_login' );
									?>

									*/ ?>
								</div>
							
								<div class="custom-login__help">
									<? include( locate_template('/includes/partials/help-text.php') ); ?>
								</div>
							</div>

							<?
								$pagename = isset($_REQUEST['action']) && $_REQUEST['action'] === 'lostpassword' ? 'forgotten-password' : 'login';

								$args = array(
									'pagename' => $pagename
								);

								$page_query = new WP_Query($args);

								if( $page_query->have_posts() ):
									while( $page_query->have_posts() ): $page_query->the_post(); ?>

										<? 
											// Include content from archive page
											include( locate_template('/includes/flexible-content.php') ); 
										?>

									<? endwhile;
								endif;
							?>
						</div>
					</div>
				</div>
			</div>

			<? include( locate_template('/includes/footer.php') );
		}
		add_action( 'login_footer', 'include_login_footer' );

	// Remove login title
		function my_login_logo_url_title() {
			return '';
		}
		add_filter( 'login_headertitle', 'my_login_logo_url_title' );


	// Redirect logins to the user dashboard
		function my_login_redirect( $redirect_to, $request, $user ) {
			//is there a user to check?
			if (isset($user->roles) && is_array($user->roles)) {
				//check for subscribers
				if (in_array('subscriber', $user->roles)) {
					// redirect them to another URL, in this case, the homepage 
					$redirect_to =  '/dashboard/?loggedin=1';
				}
			}

			return $redirect_to;
		}

		add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

		// When the user logs in, update the "last_login" value in the DB (output in users section via Admin Columns)
		function user_last_login( $user_login, $user ) {
			update_user_meta( $user->ID, 'last_login', time() );
		}
		add_action( 'wp_login', 'user_last_login', 10, 2 );


	// Redirect login page to wp-login (not needed as we now user the WPS Hide Login plugin to make /login the login page)
		/*
			function redirect_pages_to_wordpress() {

				// Check if we're on the login page, and ensure the action is not 'logout'
				if( is_page('login') ) {
					wp_redirect( '/wp-login.php' );
					exit();
				}
			}
			add_action('template_redirect', 'redirect_pages_to_wordpress');
		*/

		function redirect_to_custom_reset() {
			// Redirect Wordpress' password reset page to our custom one
			global $pagenow;

			if ( $pagenow == 'wp-login.php' && isset( $_REQUEST[ 'action' ] ) && $_REQUEST[ 'action' ] == 'lostpassword' && ! isset( $_REQUEST[ 'skip' ] ) ) {
				wp_redirect( '/reset-your-password' );

				exit();
			}
		}
		add_action('init', 'redirect_to_custom_reset');


	/* ==== Disabled users - don't allow them to login ==== */

		// Deny access to disabled users
		add_filter( 'init', 'check_custom_authentication', 10, 3 );

		function check_custom_authentication ( $username ) {
			$user = wp_get_current_user();
			
			if (isset($user->roles) && is_array($user->roles)) {
				//check for subscribers
				if (in_array('disabled', $user->roles)) {
					// redirect them to another URL, in this case, the homepage
					$logout_url = wp_login_url().'?access=denied';
					wp_logout();
					wp_redirect( $logout_url, 302 );
					exit;
				}
			}
		}

		// Show message if users account is disabled
		function my_login_message() {

			if( isset($_GET['access']) && $_GET['access'] == 'denied' ){
				$message = '<p><strong>Your account is not currently allowed access to the Jersey Finance website.</strong></p>';
				return $message;
			}

		}
		add_filter('login_message', 'my_login_message');
	
	/* === END === */

	/* ==== Customise social login authentication page (hide registration options, only allow account linking) ==== */

		function hide_social_registration() { ?>
			<link rel='stylesheet' id='font_stylesheet-css'  href='//fast.fonts.net/cssapi/cdd45045-3e98-480a-97ce-0f1aae95dc86.css?ver=5.0.3' type='text/css' media='all' />
			<link rel="stylesheet" href="https://www.jerseyfinance.je/wp-content/themes/jerseyfinance/dist/css/style.css?ver=1.21.84" />
			
			<style>
				* {
					box-sizing: border-box;
				}

				#login {
					padding: 70px 0 0 !important;
				}

				#login #login-panel {
					padding-bottom: 0;
				}

				#login table {
					margin-bottom: 0;
				}

				#login #avatar {
					background-color: #FFF;
					background-image: url('/wp-content/themes/jerseyfinance/dist/images/jfl-lion.svg');
					background-size: contain;
					background-repeat: no-repeat;
					background-position: 50% 45%;
					background-size: 70%;
					border: 1px solid #f1f1f1;
					border-radius: 50%;
					box-shadow: 0 1px 3px rgba(0, 0, 0, 0.13);
					height: 110px;
					width: 110px;
					margin-left: 0;
					top: 0px;
					left: 50%;
					transform: translate(-50%, -50%);
				}

				#avatar img {
					display: none;
				}

				#login {
					padding-left: 15px !important;
					padding-right: 15px !important;
				}

				#idp-icon {
					display: none;
				}

				#login h4 {
					display: none;
				}

				#login table {
					background: #FFF;
				}

				#login table p {
					font-size: 14px !important;
				}

				#login table td {
					border: 0;
				}

				#login b {
					display: block;
					margin-bottom: 10px;
				}

				#login p {
					display: block;
					margin-bottom: 20px;
				}

				#login, #mapping-options {
					width: 100% !important;
					max-width: 600px;
				}

				#mapping-options > tbody > tr > td {
					display: none;
				}

				#mapping-options > tbody > tr > td:first-child {
					display: block;
					width: 100%;
				}

				#login .button-primary {
					background: transparent;
					border-width: 2px;
					border-style: solid;
					border-radius: 3px;
					box-shadow: none;
					cursor: pointer;
					display: inline-block;
					font-size: 0.75rem;
					font-weight: 700;
					letter-spacing: 0.125rem;
					line-height: 1rem;
					margin-top: 10px;
					margin-bottom: 10px;
					padding: 16px 30px;
					position: relative;
					text-decoration: none;
					text-transform: uppercase;
					transition: color 0.2s linear 0.35s, background-position 0.25s ease-in-out;

					border-color: #CB181C;
					color: #CB181C;
					background-image: linear-gradient(to left, transparent, transparent 50%, #CB181C 50%, #CB181C);
					background-position: 100% 0;
					background-size: 210% 100%;

					background-color: #CB181C;
					color: #FFF;

					height: auto;
				}

				.back-to-home a {
					font-size: 14px;
				}
			</style>
		<? }
		add_action('wsl_process_login_new_users_gateway_start', 'hide_social_registration');

	/* === END === */

?>
<?
	/*
		This file basically copies across some functionality from Wordpress multisite.
		
		On a multisite installation if a user updates their email address they are sent an email that has a verification link + a hash that then makes the change.

		We have a function below that generates the email if the user has changed their email address. 
		It contains the url needed to verify the change.

		The other function is the one that verifies the link and updates the users email.
	*/

	// Stop Wordpress from sending out their normal email (all it does it tell the user it's changed - not very helpful)
	add_filter( 'send_email_change_email', '__return_false' );


	/* 
		When the user updates their details, check if their email has changed:
		If it has, save new email to new DB row, along with a hash for validation and send out the verification email.
		The email contains a link to our custom `verify-email` endpoint that checks if the hash in the email matched the one we set in the DB.
	*/		
		function custom_send_confirmation_on_profile_email( $entry ) {
			global $user_update_fields;

			// Get the current user
			$current_user = wp_get_current_user();

			// Get the current email address
			$current_email = $current_user->user_email;

			// Get the current user ID (we use this so the user can reset their pass without needing to be logged in)
			$user_id = $current_user->ID;

			// Get the email they submitted
			$new_email = $entry[$user_update_fields['email']['entry_key']];

			// If the emails don't match, they are changing their email address
			if( $new_email !== $current_email ):
				
				// Push the new email address into a temporary email field along with a hash for validation
				$hash = md5( $new_email . time() . mt_rand() );
				
				$new_user_email = array(
					'hash' 			=> $hash,
					'newemail' 		=> $new_email
				);

				update_field( '_new_email', $new_user_email, 'user_' . $user_id );

				// Generate a notification that informs the user someone has requested a change

				$content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Jersey Finance</title><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body style="-webkit-font-smoothing:antialiased;width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;margin:0;"><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr><td class="background-cell" bgcolor="#F5F5F5" align="center" style="padding:0px"><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px" class="responsive-table"><tr><td bgcolor="#ffffff" class="fw-cell" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt"><table class="fw-table" width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" bgcolor="#CB181C" style="padding:15px 15px 5px 15px;text-align:right;font-size:9px;color:#ffffff"></td></tr><tr><td align="center" bgcolor="#CB181C" style="padding:0px 30px 40px 30px;" valign="top"><div class="h-pad-xs"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="col grid-image-cell-stacked" width="200" valign="top" style="padding-right:10px"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><a href="https://www.jerseyfinance.je/" target="_blank"><img class="masthead-logo" src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/jfl_logo_masthead_ls.png" width="200" height="31" alt="Jersey Finance" border="0" style="display:block; margin:0 auto;"></a></td></tr></table></td><td class="col" valign="middle" width="240" valign="top" style="padding-left:60px;padding-right:20px;padding-top:10px"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><a href="https://www.jerseyfinance.je/" target="_blank"><img class="responsive-image" src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/jfl_strapline_masthead.png" width="246" height="12" alt="Delivering Insight" border="0" style="display:block; margin:0 auto;max-width:246px !important;"></a></td></tr></table></td></tr></table></div></td></tr></table></td></tr></table></td></tr><tr><td align="center" class="content-container" bgcolor="#ffffff" style="padding:15px 0px 15px 0px"><div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="responsive-cell"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="content-cell-p-h-1" style="text-align:center"><h1>Confirm the change of email address for your JerseyFinance.je website account</h1></td></tr></table></td></tr></table></div><div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="responsive-cell"><table width="100%" cellpadding="20" cellspacing="0" border="0"><tr><td class="content-cell"><div class="content-cell-p-h-1">';
				
				$content .= apply_filters( 'new_user_email_content', __( "
					<p>There has been a request on the Jersey Finance website to have the email address on an account changed from ###EMAIL### to ###NEW_EMAIL###.</p>

					<p>If you requested this change, please click on the following link to confirm it:
					<a href='###ADMIN_URL###'>###ADMIN_URL###</a></p>

					<p>If you did not request this, you can safely ignore and delete this email and no action will be taken.</p>

					<p>This email has been sent to ###NEW_EMAIL###</p>
				" ), $new_user_email );

				$content .= '</div></td></tr></table></td></tr></table></div><br/></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:40px 30px 5px 30px"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/jfl_footer_logo.png" alt="Jersey Finance" width="66" height="46"></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:15px 15px 15px 15px;"><div style="display:block;max-width:315px"><p style="color:#4A4A4A;font-size:16px;line-height:26px">Promoting Jersey as the clear leader in international finance</p></div></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:15px 0px 40px 0px"><table cellpadding="0" cellspacing="0" border="0"><tr><td align="center" valign="middle" class="socialIcon"><a href="http://twitter.com/jerseyfinance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_twitter.png" width="20" height="20" alt="twitter" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="http://www.linkedin.com/company/jersey-finance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_linkedin.png" width="20" height="20" alt="linkedin" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="http://www.youtube.com/user/JerseyFinance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_youtube.png" width="20" height="20" alt="youtube" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="http://www.facebook.com/jerseyfinanceeducation/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_facebook.png" width="20" height="20" alt="facebook" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="https://www.instagram.com/jerseyfinance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_instagram.png" width="20" height="20" alt="instagram" border="0"></a></td></tr></table></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:0px 0px 15px 0px"><a class="button" href="https://www.jerseyfinance.je/">Contact Us</a></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:15px 30px 60px 30px"><p style="font-size:12px;color:#CB181C;"><a href="https://www.jerseyfinance.je/terms-conditions/" target="blank">Terms &amp; Conditions</a> |<a style="font-size:12px;color:#CB181C;text-decoration:none" href="https://www.jerseyfinance.je/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="https://www.jerseyfinance.je/cookie-policy/" target="_blank" style="font-size:12px;color:#CB181C;text-decoration:none">Cookie Policy</a><br></p></td></tr><tr><td align="center" bgcolor="#ffffff" class="content-cell" style="padding:25px 30px 25px 30px"><p style="font-size:12px">Copyright &copy; Jersey Finance Limited<br></p></td></tr></table></td></tr></table></body></html>';

				$headers = array('Content-Type: text/html; charset=UTF-8');

				$content = str_replace( '###ADMIN_URL###', esc_url( get_site_url() . '/verify-email/?newuseremail=' . $hash . '&id=' . $user_id ), $content );
				$content = str_replace( '###EMAIL###', $current_email, $content);
				$content = str_replace( '###NEW_EMAIL###', $new_email, $content);

				wp_mail( $new_email, sprintf( __( '[%s] New Email Address' ), get_option( 'blogname' ) ), $content, $headers );

			endif;
		}
		add_action( 'gform_after_submission_4', 'custom_send_confirmation_on_profile_email' );

	/* 
		Check if hash in URL matches the hash set by the above function.
		If it does - execute email change and remove temporary DB data + redirect to login.
	*/

		function verify_email_change( $query ){			
			global $errors, $wpdb;

			// Hashed timestamp + user ID from query string
			$newuseremail 	= filter_input( INPUT_GET, 'newuseremail', FILTER_SANITIZE_SPECIAL_CHARS );
			$user_id 		= filter_input( INPUT_GET, 'id', FILTER_VALIDATE_INT ); // INT only

			// If we are on my custom verify-email endpoint
			if (array_key_exists('verify-email', $query->query_vars)):
				
				// And the query string has the hashed email token and user ID in it
				if ( isset( $newuseremail ) && isset( $user_id ) ):

					// Fetch the user data using the ID
					$current_user = get_userdata($user_id);

					// If the user exists
					if( !empty($current_user) ):

						// Get the email + hash from the DB of the selected user
						$email_and_hash 	= get_user_meta( $user_id , '_new_email' );

						// If there is meta in there, they have requested a change, so continue
						if( !empty($email_and_hash) ):
							$email_and_hash 	= $email_and_hash[0]; // first one?

							// If the hash in the DB matches the one from the query string
							if ( $email_and_hash['hash'] == $newuseremail ):

								// If there is a new email address  in the DB
								if ( !empty( $email_and_hash['newemail'] ) ) :
									// Modify user object, replacing their old email
									$current_user->user_email = esc_html( trim( $email_and_hash[ 'newemail' ] ) );

									// Update  the user in DB
									wp_update_user( array (
										'ID' => $current_user->ID, 
										'user_email' => esc_attr( $current_user->user_email ) 
									));
								endif;

								// Re-assign user to businesses
								add_user_to_businesses( $current_user->ID );


								
								// Remove the saved "_new_email" value
								delete_user_meta( $current_user->ID , '_new_email' ); 

								wp_logout();					// Log the user out
								wp_redirect( '/login' );	// Redirect the user to the login screen
								die();

								// Hash doesn't match
							else:
								wp_redirect( '/404' );	// Redirect the user to the login screen
							endif;

						// The user hasn't requested a new email, so 404
						else:
							wp_redirect( '/404' );	// Redirect the user to the login screen
							die();
						endif;

					endif;

				// If the user has chosen to dismiss the email change (I don't think they will happen as this has to be done from the admin)
				elseif ( !empty( $_GET['dismiss'] ) && get_user_meta($current_user->ID , '_new_email') == $_GET['dismiss'] ):
					// Remove the saved "_new_email" value
					delete_user_meta( $current_user->ID . '_new_email' );

					// wp_logout();					// Log the user out
					// wp_redirect( '/login' );	// Redirect the user to the login screen
					// die();
				endif;
			endif;
		}
		add_action('pre_get_posts','verify_email_change');
?>
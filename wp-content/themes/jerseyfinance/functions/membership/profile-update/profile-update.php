<?
	function after_profile_update( $entry, $form ) {
		global $user_update_fields;

		// echo '<pre>';
		// print_r($entry);
		// echo '</pre>';
		
		// Set the send_to_salesforce value to 1 so that it gets synced up with Salesforce
		$user_id = get_current_user_id();
		update_user_meta($user_id, 'send_to_salesforce', '1');
	}
	add_action( 'gform_after_submission_4', 'after_profile_update', 10, 2 );		

	// Prefill the users email address - we aren't updating it from GF so we need to prefill manually
	function prefill_user_email( $value ) {
		$current_user = wp_get_current_user();

		// Get the current email address
		$current_email = $current_user->user_email;
		return $current_email;
	}
	add_filter( 'gform_field_value_prefill_user_email', 'prefill_user_email' );


	function inlude_checkbox_script() {
		// Add the below scripts to the footer, if we are in form #4
		function show_hide_checkboxes() {
			global $user_update_fields; ?>

				<script>
					var emailField 		= $('#input_4_<? echo $user_update_fields['email']['input_id']; ?>');
					var countryField 		= $('#input_4_<? echo $user_update_fields['country']['input_id']; ?>');

					function refresh_checkboxes() {
						var userEmail 			= emailField.val();
						var userCountry 		= countryField.val();

						// Hide the last two sets of checkboxes if the user has no business matches
						var newsletters 	= $("#field_4_<? echo $user_update_fields['newsletters']['input_id']; ?>");
						var hear_about 	= $("#field_4_<? echo $user_update_fields['hear_about']['input_id']; ?>");

						newsletters.hide();
						hear_about.hide();

						if( userEmail && userCountry == 'Jersey' ) {
							// Generate the URL to our JSON
							var ajaxUrl = '/json/businesses?email=' + userEmail;

							ajaxBusinesses = $.ajax({
								'url': 	ajaxUrl,
								dataType: 'json',
								cache: 	true
							})
							.done(function(data) {
								// If any businesses match, show additional checkboxes on update form
								if( Object.keys(data).length ) {
									newsletters.show();
									hear_about.show();
								} else {
									newsletters.hide();
									hear_about.hide();
									
									// Untick everything
									newsletters.find(':checkbox').prop('checked', false).prop('selected', false);
									// Untick everything
									hear_about.find(':checkbox').prop('checked', false).prop('selected', false);
								}
							});
						}
					}

					$(document).on('gform_post_render', function(){
						refresh_checkboxes();

						emailField.change(function() {

							setTimeout( function() {
								countryField.trigger('change');
							}, 150);
						});

						// When the country field is changed, fetch a JSON feed to businesses that match the users email address
						countryField.change(function() {

							refresh_checkboxes();
						});
					});
				</script>

	<? } add_action('wp_footer', 'show_hide_checkboxes', 20);
}
add_action( 'gform_enqueue_scripts_4', 'inlude_checkbox_script', 10, 2 );
<? 
	function fetch_favourites() {
		// Get existing favourites
		$favourites = get_user_meta( get_current_user_ID(), 'favourites', true );

		// If there are no favourites then set as empty array
		$favourites = !empty($favourites) ? $favourites : array();

		return $favourites;
	}
?>
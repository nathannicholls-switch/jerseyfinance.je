<?
	add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', function () {

		/* === Post based security === */
			$meta_query = [];

			// Only fetch posts taht have some level of security
			$meta_query[] = array(
				'key' 		=> 'members_only',
				'value'		=> array('guest', 'full', 'ceo'),
				'compare'	=> 'IN'
			);

			$args = array(
				'posts_per_page' => -1,
				'post_type'	=> array(
					'all-news', 
					'all-events', 
					'all-work', 
					'pages'
				),
				'meta_query' => $meta_query
			);

			$secured_posts = new WP_Query( $args );
		
		/* === END === */

		$excluded_ids = array_column( $secured_posts->posts, 'ID' );

		return $excluded_ids;

	});

?>